<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'web'], function() {
	// First loaded page
	Route::get('/', function() {
		return redirect('/lv');
	});
	
	// Authentication Routes...
	Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
	Route::post('login', 'Auth\LoginController@login');
	Route::post('logout', 'Auth\LoginController@logout')->name('logout');

	// Registration Routes...
	Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register')->middleware('auth');
	Route::post('register', 'Auth\RegisterController@register')->middleware('auth');

	// Password Reset Routes...
	/*Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
	Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
	Route::post('password/reset', 'Auth\ResetPasswordController@reset');*/
	
	// Admin side routes
	Route::get('/admin/home', ['uses' => 'AdminController@index', 'as' => 'home'])->name('home')->middleware('auth');
	Route::post('/admin/home/save{subitem?}', ['uses' => 'AdminController@homeSave', 'as' => 'homeSave'])->name('homeSave')->middleware('auth');
	Route::get('/admin/about-us', ['uses' => 'AdminController@about', 'as' => 'about'])->name('about')->middleware('auth');
	Route::post('/admin/about-us/save{subitem?}', ['uses' => 'AdminController@aboutSave', 'as' => 'aboutSave'])->name('aboutSave')->middleware('auth');
	Route::get('/admin/pricelist', ['uses' => 'AdminController@pricelist', 'as' => 'pricelist'])->name('pricelist')->middleware('auth');
	Route::post('/admin/pricelist/save', ['uses' => 'AdminController@pricelistSave', 'as' => 'pricelistSave'])->name('pricelistSave')->middleware('auth');
	Route::get('/admin/specialists', ['uses' => 'AdminController@specialists', 'as' => 'adminSpecialists'])->name('adminSpecialists')->middleware('auth');
	Route::get('/admin/specialists/specialist/edit{subitem?}', ['uses' => 'AdminController@specialists', 'as' => 'adminSpecialists'])->name('adminSpecialists')->middleware('auth');
	Route::post('/admin/specialists/specialist/save{subitem?}', ['uses' => 'AdminController@specialistSave', 'as' => 'adminSpecialistSave'])->name('adminSpecialistSave')->middleware('auth');
	Route::get('/admin/specialists/specialist/delete_img{subitem?}', ['uses' => 'AdminController@specialistImgDelete', 'as' => 'specialistImgDelete'])->name('specialistImgDelete')->middleware('auth');
	Route::get('/admin/specialists/specialist/services/edit{subitem?}', ['uses' => 'AdminController@specialistServices', 'as' => 'specialistServices'])->name('specialistServices')->middleware('auth');
	Route::post('/admin/specialists/specialist/services/save{subitem?}', ['uses' => 'AdminController@specialistServicesSave', 'as' => 'specialistServicesSave'])->name('specialistServicesSave')->middleware('auth');
	Route::get('/admin/services', ['uses' => 'AdminController@services', 'as' => 'services'])->name('services')->middleware('auth');
	Route::post('/admin/services/save', ['uses' => 'AdminController@servicesSave', 'as' => 'servicesSave'])->name('servicesSave')->middleware('auth');
	Route::get('/admin/services/service/edit{subitem?}', ['uses' => 'AdminController@serviceEdit', 'as' => 'serviceEdit'])->name('serviceEdit')->middleware('auth');
	Route::post('/admin/services/service/save{subitem?}', ['uses' => 'AdminController@serviceSave', 'as' => 'serviceSave'])->name('serviceSave')->middleware('auth');
	Route::get('/admin/services/service/delete{subitem?}', ['uses' => 'AdminController@serviceDelete', 'as' => 'serviceDelete'])->name('serviceDelete')->middleware('auth');
	Route::get('/admin/services/subservice/edit{subitem?}', ['uses' => 'AdminController@subServiceEdit', 'as' => 'subServiceEdit'])->name('subServiceEdit')->middleware('auth');
	Route::post('/admin/services/subservice/save{subitem?}', ['uses' => 'AdminController@subServiceSave', 'as' => 'subServiceSave'])->name('subServiceSave')->middleware('auth');
	Route::get('/admin/services/subservice/delete{subitem?}', ['uses' => 'AdminController@subServiceDelete', 'as' => 'subServiceDelete'])->name('subServiceDelete')->middleware('auth');
	Route::get('/admin/shop', ['uses' => 'AdminController@shop', 'as' => 'adminShop'])->name('adminShop')->middleware('auth');
	Route::get('/admin/shop/product/edit{subitem?}', ['uses' => 'AdminController@shop', 'as' => 'adminShop'])->name('adminShop')->middleware('auth');
	Route::post('/admin/shop/product/save{subitem?}', ['uses' => 'AdminController@shopSave', 'as' => 'shopSave'])->name('shopSave')->middleware('auth');
	Route::get('/admin/shop/product/delete_img{subitem?}', ['uses' => 'AdminController@shopProdImgDelete', 'as' => 'shopProdImgDelete'])->name('shopProdImgDelete')->middleware('auth');
	Route::get('/admin/courses', ['uses' => 'AdminController@courses', 'as' => 'adminCourses'])->name('adminCourses')->middleware('auth');
	Route::get('/admin/courses/course/edit{subitem?}', ['uses' => 'AdminController@courses', 'as' => 'adminCourses'])->name('adminCourses')->middleware('auth');
	Route::post('/admin/courses/course/save{subitem?}', ['uses' => 'AdminController@courseSave', 'as' => 'courseSave'])->name('courseSave')->middleware('auth');
	Route::post('/admin/courses/course/delete{subitem?}', ['uses' => 'AdminController@courseDelete', 'as' => 'courseDelete'])->name('courseDelete')->middleware('auth');
	Route::post('/admin/courses/save', ['uses' => 'AdminController@coursesSave', 'as' => 'coursesSave'])->name('coursesSave')->middleware('auth');
	Route::get('/admin/bach-flowers', ['uses' => 'AdminController@bachFlowers', 'as' => 'bachFlowers'])->name('bachFlowers')->middleware('auth');
	Route::post('/admin/bach-flowers/save{subitem?}', ['uses' => 'AdminController@bachFlowersSave', 'as' => 'bachFlowersSave'])->name('abachFlowersSave')->middleware('auth');
	Route::get('/admin/contacts', ['uses' => 'AdminController@contacts', 'as' => 'adminContacts'])->name('adminContacts')->middleware('auth');
	Route::post('/admin/contacts', ['uses' => 'AdminController@contactsSave', 'as' => 'contactsSave'])->middleware('auth');
	Route::get('/admin/transport/delete{subitem?}', ['uses' => 'AdminController@stopDelete', 'as' => 'stopDelete'])->middleware('auth');
	Route::post('/admin/transports{subitem?}', ['uses' => 'AdminController@stopEdit', 'as' => 'stopEdit'])->middleware('auth');
	Route::get('/admin/users', ['uses' => 'AdminController@users', 'as' => 'adminUsers'])->name('adminUsers')->middleware('auth');
	Route::post('/admin/users/save{subitem?}', ['uses' => 'AdminController@userSave', 'as' => 'adminUserSave'])->name('adminUserSave')->middleware('auth');
	Route::post('/admin/users/delete{subitem?}', ['uses' => 'AdminController@userDelete', 'as' => 'adminUserDelete'])->name('adminUserDelete')->middleware('auth');
	
	// User side routes
	Route::get('/{subitem?}', ['uses' => 'HomeController@index', 'as' => 'home'])->name('home');
	Route::get('/{subitem?}/home', ['uses' => 'HomeController@index', 'as' => 'home'])->name('home');
	Route::get('/{subitem?}/about-us', ['uses' => 'HomeController@about', 'as' => 'about'])->name('about');
	Route::get('/{subitem?}/services', ['uses' => 'HomeController@services', 'as' => 'services'])->name('services');
	Route::get('/{subitem?}/services/service{subid?}', ['uses' => 'HomeController@serviceList', 'as' => 'serviceList'])->name('serviceList');
	Route::get('/{subitem?}/pricelist', ['uses' => 'HomeController@pricelist', 'as' => 'pricelist'])->name('pricelist');
	Route::get('/{subitem?}/specialists', ['uses' => 'HomeController@specialists', 'as' => 'specialists'])->name('specialists');
	Route::get('/{subitem?}/specialists/specialist{subid?}', ['uses' => 'HomeController@specialists', 'as' => 'specialists'])->name('specialistOpen');
	Route::get('/{subitem?}/shop', ['uses' => 'HomeController@shop', 'as' => 'shop'])->name('shop');
	Route::get('/{subitem?}/shop/product{subid?}', ['uses' => 'HomeController@shop', 'as' => 'shop'])->name('shop');
	Route::get('/{subitem?}/courses', ['uses' => 'HomeController@courses', 'as' => 'courses'])->name('courses');
	Route::get('/{subitem?}/courses/course{subid?}', ['uses' => 'HomeController@courses', 'as' => 'courses'])->name('courses');
	Route::get('/{subitem?}/bach-flowers', ['uses' => 'HomeController@bachFlowers', 'as' => 'bachFlowers'])->name('bachFlowers');
	Route::get('/{subitem?}/contacts', ['uses' => 'HomeController@contacts', 'as' => 'contacts'])->name('contacts');	
	
	//Undefined routes
	Route::get('/{subitem?}', function() {
		return redirect('/lv/home');
	});
});