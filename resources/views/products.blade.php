@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>
		@foreach ($menu as $menu_elem)
			@if (($menu_elem->name == $currentUrl) || ($menu_elem->name == ltrim($currentUrl, '/')))
				{{ $menu_elem->title }}
			@endif
		@endforeach
	</h2><br>
	<div class="container">
		<ul class="list-unstyled components">
			@foreach ($products as $p)
				<?php $serv_name = "" ?>
				@if ($p->serv_name != $serv_name)
					<?php $serv_name = $p->serv_name ?>
					<li  class="serv_list mt5">
						<a href="#" data-target="#prod_list_{{ $p->serv_id }}" data-toggle="collapse">
							{{ $p->serv_name }} <span class="caret"></span>
						</a>
						<div class="collapse container prod-blocks" id="prod_list_{{ $p->serv_id }}">
				@endif
							
								<a href="/{{$lang}}/shop/product?id={{$p->id}}">
									<div class="product">
										@if ($p->path=="")
											<img style="width:180px;" src="{{ asset('/images/profile/human.png') }}" alt="{{ $p->name }}"/>
										@else
											<img style="width:180px;" src="{{ asset($p->path) }}" alt="{{ $p->name }}"/>
										@endif
										<br><p>{{ $p->name }}<br><i>({{ $p->price }} EUR)</i></p>
									</div>
								</a>
				@if ($p->serv_name != $serv_name)
						</div>
					</li>
				@endif
			@endforeach
		</ul>
	</div>
</div>
@endsection
