@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>
		@foreach ($menu as $menu_elem)
			@if (($menu_elem->name == $currentUrl) || ($menu_elem->name == ltrim($currentUrl, '/')))
				{{ $menu_elem->title }}
			@endif
		@endforeach
	</h2>
	<div class="container mt50">
		@foreach ($menu as $menu_elem)
			@if (($menu_elem->name == "services"))
				<h3>{{ $menu_elem->title }}</h3>
			@endif
		@endforeach
		<div class="ml15">
			<div class="table-responsive">
				<table class="table">
					<?php $serv_name = ""; ?>
					@foreach ($services as $S)
						<tr>
							@if ($serv_name != $S->servName)
								<tr>
									<?php $serv_name = $S->servName ?>
									<th colspan="3" class="price_l"><a class="tdn" href="/{{ $lang }}/services/service?serviceId={{ $S->servId }}"><i>{{ $S->servName }}</i></a></th>
								</tr>
							@endif
							@if ($S->duration == "0")
								<td class="pl50 tal price_l" colspan="2">
							@else
								<td class="pl50 tal price_l">
							@endif
								<a class="tdn pl15" href="/{{ $lang }}/services/service?subServiceId={{ $S->subId }}">{{ $S->subName }}
							</td>
							@if ($S->duration != "0")
								<td class="tar">{{ $S->duration }} min</td>
							@endif
							<td class="tar w125"><i>
								@if ($S->price != "0")
									@if ($S->min_price == 1)
										{{ $transl }} 
									@endif
									({{ $S->price }} EUR)</i>
								@else
									{{ $agreem }}
								@endif
								</a>
							</td>
						</tr>
					@endforeach
					<tr><td colspan="3"></td></tr>
				</table>
			</div>
		</div>
		@foreach ($menu as $menu_elem)
			@if (($menu_elem->name == "courses"))
				<h3>{{ $menu_elem->title }}</h3>
			@endif
		@endforeach
		<div class="ml15">
			<div class="table-responsive">
				<table class="table">
					@foreach ($courses as $C)
						<tr>
							<td class="pl50 tal price_l">
								<a class="tdn" href="/{{ $lang }}/courses/course?courseId={{ $C->subId }}">{{ $C->subName }}
							</td>
							<td class="tar">{{ $C->duration }} min</i></td>
							<td class="tar w125"><i>
								@if ($C->price != "0")
									@if ($C->min_price == 1)
										{{ $transl }} 
									@endif
									({{ $C->price }} EUR)</i>
								@else
									{{ $agreem }}
								@endif
								</a>
							</td>
						</tr>
					@endforeach
					<tr><td colspan="3"></td></tr>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
