@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>{{ $service->title }}</h2>
	<div class="listOfServices">
		<ul class="service-list">
			@foreach ($subserv as $subServ)
				@if ($subServ->hide != 1)
					<li><a href="/{{ $lang }}/{{ $currentUrl }}/service?subServiceId={{ $subServ->id }}">{{ $subServ->title }}</a></li>
				@endif
			@endforeach
		</ul>
	</div><br><br><br>
	<div class="container">
	{!! $service->description !!}
	</div>
</div>
@endsection
