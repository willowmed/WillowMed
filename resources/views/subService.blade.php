@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2><a class="tdn" href="/{{ $lang }}/{{ $currentUrl }}/service?serviceId={{ $service->id }}">{{ $service->title }}</a></h2>
	<div class="container">
		<h3>{{ $subserv->title }}</h3>
		<table class="ml50">
			@if ($spec != "")
				<tr class="m0 thrpr10">
					<th>
						@foreach ($menu as $menu_elem)
							@if ($menu_elem->name == 'specialists')
								{{ $menu_elem->title }}: 
							@endif
						@endforeach
					</th>
					<td>{!! $spec !!}</td>
				</tr>
			@endif
			@if ($subserv->duration != "0")
				<tr class="m0 thrpr10">
					<th>{{ $dur }}: </th>
					<td>{{ $subserv->duration }} min</td>
				</tr>
			@endif
			<tr class="m0 thrpr10">
				<th>{{ $price }}: </th>
				@if ($subserv->price != "0")
					<td>
						@if($subserv->min_price == 1)
							{{ $from }} 
						@endif
						{{ $subserv->price }} EUR
					</td>
				@else
					<td>
						{{ $agreem }}
					</td>
				@endif
			</tr>
		</table>
	</div><br><br>
	<div class="container">
		{!! $subserv->info !!}
	</div>
</div>
@endsection
