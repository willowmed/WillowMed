@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>
		@foreach ($menu as $menu_elem)
			@if (($menu_elem->name == $currentUrl) || ($menu_elem->name == ltrim($currentUrl, '/')))
				{{ $menu_elem->title }}
			@endif
		@endforeach
	</h2>
	<div class="container">
		<table class="mt30">
			@foreach ($footer as $elem)
				<tr>
					<td class="contact_key">{{ $elem->infoKey }}:</td>
					<td class="contact_value">
						@foreach ($elem->info as $el)
							{{ $el->infoVal }} <br>
						@endforeach
					</td>
				</tr>
			@endforeach
		</table>
		<h3 class="tac"><br>
			@foreach ($transl as $tr)
				@if ($tr->infoKey == 'location')
					{{ $tr->infoVal }}:
				@endif
			@endforeach
		</h3>
		<div id="map"></div>
		<script>
		  function initMap() {
			var uluru = {lat: 56.9550616, lng: 24.152808899999968};
			var map = new google.maps.Map(document.getElementById('map'), {
			  zoom: 15,
			  center: uluru
			});
			var marker = new google.maps.Marker({
			  position: uluru,
			  map: map
			});
		  }
		</script>
		<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNr-l2vzrTnbpVFAGtxqSTmELFaERx-Qc&callback=initMap">
		</script>
		<div class="table-responsive">
			<h3 class="center-text tac"><br>
				@foreach ($transl as $tr)
					@if ($tr->infoKey == 'public_transport')
						{{ $tr->infoVal }}:
					@endif
				@endforeach
			</h3>
			<table class="transport" align="center">
				<tr style="border-top: 1px solid #83c341 !important; border-bottom: 1px solid #83c341 !important;">
				<?php $total = $bus + $trol + $tram - 1 ?>
					@if ($bus > 0)
						<th rowspan="{{ $bus }}"><img id="transport" src="{{ asset('/images/bus.png') }}" alt=""></th>
						@foreach ($transp as $tr)
							@if ($tr->type == 1)
								<td>{{ $tr->numbers }}</td>
								<td>{{ $tr->stop }}</td>
								@if ($total > 0) 
									</tr><tr style="border-bottom: 1px solid #83c341 !important;">
									<?php $total = $total - 1 ?>
								@endif
							@endif
						@endforeach
					@endif
					@if ($trol > 0)
						<th rowspan="{{ $trol }}"><img id="transport" src="{{ asset('/images/trolybus.png') }}" alt=""></th>
						@foreach ($transp as $tr)
							@if ($tr->type == 2)
								<td>{{ $tr->numbers }}</td>
								<td>{{ $tr->stop }}</td>
								@if ($total > 0) 
									</tr><tr style="border-bottom: 1px solid #83c341 !important;">
									<?php $total = $total - 1 ?>
								@endif
							@endif
						@endforeach
					@endif
					@if ($tram > 0)
						<th rowspan="{{ $tram }}"><img id="transport" src="{{ asset('/images/tram.png') }}" alt=""></th>
						@foreach ($transp as $tr)
							@if ($tr->type == 3)
								<td>{{ $tr->numbers }}</td>
								<td>{{ $tr->stop }}</td>
								@if ($total > 0) 
									</tr><tr style="border-bottom: 1px solid #83c341 !important;">
									<?php $total = $total - 1 ?>
								@endif
							@endif
						@endforeach
					@endif
				</tr>
			</table>
		</div>
		<div>
			<h3 class="center-text tac"><br>
				@foreach ($transl as $tr)
					@if ($tr->infoKey == 'free_parking')
						{{ $tr->infoVal }}:
					@endif
				@endforeach
			</h3>
			<img class="center-block" src="{{ asset('/images/free_parking.jpg') }}" alt="" style="max-width: 100%;">
		</div>
	</div>
</div>
@endsection
