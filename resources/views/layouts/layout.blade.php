<!DOCTYPE html>
<html>
	<head>
		<meta name="google-site-verification" content="<meta name='google-site-verification' content='NOKE2wLgdjJRVOkSiVq7IM3u-dLi-YkeT9wGWnkSA4s' />" />
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>WillowMed</title>
		<link rel="icon" href="{{ asset('/images/willow_med_logo_small.png') }}">

		<!-- Styles -->
		<link href="{{ asset('bootstrap-3.3.7-dist/css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('bootstrap-3.3.7-dist/css/bootstrap.css') }}" rel="stylesheet">
		<link href="{{ asset('css/design.css') }}" rel="stylesheet">
		<!-- Scripts -->
		<script src="{{ asset('js/script.js') }}"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="{{ asset('bootstrap-3.3.7-dist/js/bootstrap.min.js') }}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />

	</head>
	<body>
		<div id="app">
			<nav class="navbar navbar-default navbar-static-top">
				<div class="container">
					<div class="navbar-header">

						<!-- Collapsed Hamburger -->
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
							<span class="sr-only">Toggle Navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<!-- Branding Image -->
						<a class="navbar-brand p5" href="{{ url('/') }}">
							<img src="{{ asset('/images/willow_med_logo.png') }}" alt="WillowMed" class="willow_logo"></img>
						</a>
					</div>
					<div class="collapse navbar-collapse" id="app-navbar-collapse">
						<!-- Left Side Of Navbar -->
						<ul class="nav navbar-nav">
							&nbsp;
						</ul>

						<!-- Right Side Of Navbar -->
						<ul class="nav navbar-nav navbar-right">
							<!-- Navbar elements -->
							@foreach ($menu as $menu_elem)
								<li>
									@guest
										@if (($menu_elem->name == $currentUrl) || ($menu_elem->name == ltrim($currentUrl, '/')))
											<a class="activeMenuElem" href="/{{ $lang }}/{{ $menu_elem->name }}">{{ $menu_elem->title }}</a>
										@else
											<a class="inactiveManuElem" href="/{{ $lang }}/{{ $menu_elem->name }}">{{ $menu_elem->title }}</a>
										@endif
									@else
										@if (($menu_elem->name == $currentUrl) || ($menu_elem->name == ltrim($currentUrl, '/')))
											<a class="activeMenuElem" href="/admin/{{ $menu_elem->name }}">{{ $menu_elem->title }}</a>
										@else
											<a class="inactiveManuElem" href="/admin/{{ $menu_elem->name }}">{{ $menu_elem->title }}</a>
										@endif
									@endguest
								</li>
							@endforeach
							
							<!-- Language selelect -->
							@guest
								<li class="dropdown pl15">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ strtoupper($lang) }}<span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="{{ $lvUrl }}">LV</a></li>
										<li><a href="{{ $enUrl }}">EN</a></li>
										<li><a href="{{ $ruUrl }}">RU</a></li>
									</ul>
								</li>
							@else
								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ Auth::user()->name }}<span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="{{ route('register') }}">Reģistrēt jaunu lietotāju</a></li>
										@if (Auth::user()->super_user == 1)
											<li><a href="/admin/users">Lietotāji</a></li>
										@endif
										<li>
											<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Atslēgties</a>
											<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
										</li>
									</ul>
								</li>
							@endguest
						</ul>
					</div>
				</div>
			</nav>


			<!--Content section-->
			@yield('content')


			<!--Footer-->
			<div class="row" id="footer">
				<div class="col-xs-6">
					<table id="footer_tab">
						@foreach ($footer as $elem)
							<tr><td class="footer_key">{{ $elem->infoKey }}:</td><td class="footer_value">
							@foreach ($elem->info as $el)
								{{ $el->infoVal }} <br>
							@endforeach
							</td></tr>
						@endforeach
						<tr>
							<td colspan="2">
								<a href="#" ><img class="s24" src="{{ asset('/images/fb_32x32.png') }}" alt=""/>
							</td>
						</tr>
					</table>
				</div>
				
				<div class="col-xs-6 text-right">
					© WILLOWMED 2017
				</div>
			</div>
		</div>
	</body>
</html>
