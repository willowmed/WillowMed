@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>{{ $product->name }} <i>({{ $product->price }} EUR)</i></h2><br>
	<div class="container">
		@if ($product->path=="")
			<img style="width:180px;" src="{{ asset('/images/profile/human.png') }}" alt="{{ $product->name }}"/>
		@else
			<img style="width:180px;" src="{{ asset($product->path) }}" alt="{{ $product->name }}"/>
		@endif
		<br><br><br>
		{!! $product->desctription !!}
	</div>
</div>
@endsection
