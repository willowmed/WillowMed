@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>
		@foreach ($menu as $menu_elem)
			@if (($menu_elem->name == $currentUrl) || ($menu_elem->name == ltrim($currentUrl, '/')))
				{{ $menu_elem->title }}
			@endif
		@endforeach
	</h2>
	<div class="container spec-blocks">
		@foreach ($specialists as $spec)
			<a class="" href="/{{ $lang }}/specialists/specialist?id={{ $spec->id }}" >
				<div class="specialist">
					@if ($spec->path != "")
						<img class="spec-img" src="{{ asset($spec->path) }}" alt="{{ $spec->name }} {{ $spec->surname }}"/>
					@else
						<img class="spec-img" src="{{ asset('/images/profile/human.png') }}" alt="{{ $spec->name }} {{ $spec->surname }}"/>
					@endif
					<p><b>{{ $spec->name }} {{ $spec->surname }}</b><br><i>({{ ucfirst($spec->prof) }})</i></p>
				</div>
			</a>
		@endforeach
	</div>
</div>
@endsection
