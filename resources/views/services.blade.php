@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>
		@foreach ($menu as $menu_elem)
			@if (($menu_elem->name == $currentUrl) || ($menu_elem->name == ltrim($currentUrl, '/')))
				{{ $menu_elem->title }}
			@endif
		@endforeach
	</h2><br>
	<div class="container">
		<ul class="list-unstyled components">
			<?php 
				$serv_name = "";
				$s = 0;
			?>
			@foreach ($services as $serv)
				@if ($serv->name != $serv_name)
					@if ($s == 1) 
							</ul>
						</li>
					@endif
					<?php 
						$serv_name = $serv->name;
						$s = 1;
					?>
						<li  class="serv_list serv_list_col mt5">
							<a href="/{{ $lang }}/{{ $currentUrl }}/service?serviceId={{ $serv->servId }}">{{ $serv->name }}</a>
							<ul class="list-unstyled ml50 mt5 mb10" id="{{ $serv->servId }}">
				@endif
								<li  class="serv_sub_list"><a href="/{{ $lang }}/{{ $currentUrl }}/service?subServiceId={{ $serv->subServId }}">{{ $serv->subName }}</a></li>
			@endforeach
			
		</ul>
	</div>
</div>
@endsection
