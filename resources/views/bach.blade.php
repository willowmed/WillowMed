@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<div class="container">
		{!! $bach->content !!}
	</div>
</div>
@endsection
