@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>{{ $spec->name }} {{ $spec->surname }} <i>({{ ucfirst($spec->prof) }})</i></h2>
	<div class="spec-info container">
		<div class="specialist">
			@if ($spec->path != "")
				<img class="spec-img" src="{{ asset($spec->path) }}" alt="{{ $spec->name }} {{ $spec->surname }}"/>
			@else
				<img class="spec-img" src="{{ asset('/images/profile/human.png') }}" alt="{{ $spec->name }} {{ $spec->surname }}"/>
			@endif
		</div>
		<div class="spec-info-section">
			@foreach ($spec_section as $sect)
				<a class="link" href="#" data-rel="sec_{{ $sect->sec_id }}">
					@if ($sect->subs != "what_is")
						<h4><i>{{ $sect->name }}</i></h4>
					@else
						<h4><i>{{ $sect->name }} {{ Illuminate\Support\Str::lower($spec->prof) }}</i></h4>
					@endif
				</a>
			@endforeach
		</div>
	</div>
	<div>
		@foreach ($spec_section as $sect)
			@if ($sect->subs == "what_is")
				<div class="data" id="sec_{{ $sect->sec_id }}">
			@else
				<div class="data" id="sec_{{ $sect->sec_id }}" style="display: none">
			@endif
				<h3><i>{{ $sect->name }} 
					@if ($sect->subs == "what_is")
						{{ Illuminate\Support\Str::lower($spec->prof) }}
					@endif
				</i></h3>
				<div class="lm30 mt5 mb10">{!! $sect->info !!}</div>
			</div>
		@endforeach
	</div>
	<script>
		$(".link").click(function(e) {
			e.preventDefault();
			$('.data').hide();
			$('#' + $(this).data('rel')).fadeIn('slow');
		});
	</script>
	<div class="container mt50">
		<h3>
			@foreach ($menu as $menu_elem)
				@if ($menu_elem->name == "services")
					{{ $menu_elem->title }}
				@endif
			@endforeach
		</h3>
		<div class="ml15">
			<div class="table-responsive">
				<table class="table">
					<?php $serv_name = ""; ?>
					@foreach ($services as $S)
						<tr>
							@if ($serv_name != $S->servName)
								<tr>
									<?php $serv_name = $S->servName ?>
									<th colspan="2" class="price_l"><a class="tdn" href="/{{ $lang }}/services/service?serviceId={{ $S->servId }}">{{ $S->servName }}</a></th>
								</tr>
							@endif
							<td class="pl50 tal price_l"><a class="tdn pl15" href="/{{ $lang }}/services/service?subServiceId={{ $S->subId }}">{{ $S->subName }}</td>
							<td class="tar w125"><i>
								@if ($S->min_price == 1)
									{{ $transl }} 
								@endif
								({{ $S->price }} EUR)</i></a>
							</td>
						</tr>
					@endforeach
					<tr><td colspan="2"></td></tr>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
