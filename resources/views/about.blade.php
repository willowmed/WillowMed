@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>
		@foreach ($menu as $menu_elem)
			@if (($menu_elem->name == $currentUrl) || ($menu_elem->name == ltrim($currentUrl, '/')))
				{{ $menu_elem->title }}
			@endif
		@endforeach
	</h2>
	<div class="container mt25">
		{!! $about->content !!}
	</div>
	<div class="container"><br><br>
		@foreach ($spec_section as $sect)
			<a class="link" href="#" data-rel="sec_{{ $sect->sec_id }}"><h4><i>{{ $sect->name }}</i></h4></a>
			<div class="data" id="sec_{{ $sect->sec_id }}" style="display: none">
				<div class="mt5 mb20 ml30">{!! $sect->info !!}</div>
			</div>
		@endforeach
	</div>
	<script>
		$(".link").click(function(e) {
			e.preventDefault();
			$('.data').hide();
			$('#' + $(this).data('rel')).fadeIn('slow');
		});
	</script>
</div>
@endsection
