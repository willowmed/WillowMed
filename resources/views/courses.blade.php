@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>
		@foreach ($menu as $menu_elem)
			@if (($menu_elem->name == $currentUrl) || ($menu_elem->name == ltrim($currentUrl, '/')))
				{{ $menu_elem->title }}
			@endif
		@endforeach
	</h2><br>
	<div class="container">
		<ul class="list-unstyled components ml50">
			@foreach ($courses as $course)
				<li  class="serv_sub_list mb5">
					<a href="/{{ $lang }}/courses/course?courseId={{ $course->id }}">
						<h3 class="m0"><b><i>{{ $course->title }}</i></b></h3>
					</a>
					<table class="ml50">
						@if ($spec != "")
							<tr class="m0 thrpr10">
								<th>
									@foreach ($menu as $menu_elem)
										@if ($menu_elem->name == 'specialists')
											{{ $menu_elem->title }}: 
										@endif
									@endforeach
								</th>
								<td>{!! $spec[$course->id] !!}</td>
							</tr>
						@endif
						@if ($course->duration != "0")
							<tr class="m0 thrpr10">
								<th>{{ $dur }}: </th>
								<td>{{ $course->duration }} min</td>
							</tr>
						@endif
						<tr class="m0 thrpr10">
							<th>{{ $price }}: </th>
							@if ($course->price != "0")
								<td>
									@if($course->min_price == 1)
										{{ $from }} 
									@endif
									{{ $course->price }} EUR
								</td>
							@else
								<td>
									{{ $agreem }}
								</td>
							@endif
						</tr>
					</table>
				</li><br>
			@endforeach
		</ul>
	</div>
</div>
@endsection
