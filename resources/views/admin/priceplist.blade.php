@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>
		@foreach ($menu as $menu_elem)
			@if (($menu_elem->name == $currentUrl) || ($menu_elem->name == ltrim($currentUrl, '/')))
				{{ $menu_elem->title }}
			@endif
		@endforeach
	</h2>
	<div class="container" style="margin-top: 50px;">
		<div class="table-responsive">
			<form id="price" class="form-inline" enctype="multipart/form-data" role="form" method="POST" action="/admin/pricelist/save">
				{!! csrf_field() !!}
				<h3>Pakalpojumi</h3>
				<div class="ml15">
					<table class="table">
						<?php $serv_name = ""; ?>
						@foreach ($services as $S)
							<tr>
								@if ($serv_name != $S->servName)
									<tr>
										<?php $serv_name = $S->servName ?>
										<th colspan="3"><a style="text-decoration: none;" href="/admin/services/service?serviceId={{ $S->servId }}">{{ $S->servName }}</a></th>
									</tr>
								@endif
								<td style="padding-left: 50px; text-align: left;"><a style="text-decoration: none;" href="/admin/services/service?subServiceId={{ $S->subId }}">{{ $S->subName }}</td>
								<td style="text-align: right;"></a>
									<input type="text" style="text-align:center; font-weight: normal; width: 100px;" name="dur_{{ $S->subId }}" form="price" value="{{ $S->duration}}" placeholder="Proced. ilgums"/>
									<i>min</i>
								</td>
								<td style="text-align: right;">
									@if ($S->min_price == 1)
										no 
									@endif
									<input type="number" min="0.00" step="0.01"  style="text-align:center; font-weight: normal; width: 75px;" name="{{ $S->subId }}" form="price" value="{{ $S->price}}" placeholder="Cena"/>
									<i>EUR</i>
								</td>
							</tr>
						@endforeach
						<tr><td colspan="3"></td></tr>
					</table>
				</div>
				<h3>Kursi un apmācības</h3>
				<div class="ml15">
					<table class="table">
						@foreach ($courses as $S)
							<tr>
								<td style="padding-left: 50px; text-align: left;"><a style="text-decoration: none;" href="/admin/courses/course?courseId={{ $S->subId }}">{{ $S->subName }}</td>
								<td style="text-align: right;"></a>
									<input type="text" style="text-align:center; font-weight: normal; width: 100px;" name="dur_{{ $S->subId }}" form="price" value="{{ $S->duration}}" placeholder="Proced. ilgums"/>
									<i>min</i>
								</td>
								<td style="text-align: right;">
									@if ($S->min_price == 1)
										no 
									@endif
									<input type="number" min="0.00" step="0.01"  style="text-align:center; font-weight: normal; width: 75px;" name="{{ $S->subId }}" form="price" value="{{ $S->price}}" placeholder="Cena"/>
									<i>EUR</i>
								</td>
							</tr>
						@endforeach
						<tr><td colspan="3"></td></tr>
					</table>
				</div>
				<button style="width:100%; margin-top: 20px;" type="submit" class="btn btn-success">Saglabāt izmaiņas</button>
			</form>
		</div>
	</div>
</div>
@endsection
