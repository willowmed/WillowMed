@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>
		@foreach ($menu as $menu_elem)
			@if (($menu_elem->name == $currentUrl) || ($menu_elem->name == ltrim($currentUrl, '/')))
				{{ $menu_elem->title }}
			@endif
		@endforeach
	</h2>
	
	@if(isset($errors))
		@foreach($errors as $error)
			<p style="text-align:center; width:100%;">{{ $error }}</p>
		@endforeach
	@endif
	<form id="contacts" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="/admin/contacts">
		{!! csrf_field() !!}
		@foreach ($footer as $elem)
			<label style="text-align:center; width:100%;">{{ $elem->infoKey }}:</label>
			@foreach ($elem->info as $el)
				<input type="text" style="text-align:center; width:100%;" name="{{ $elem->infoKey }}" form="contacts" value="{{ $el->infoVal }}" placeholder="{{ $elem->infoKey }}"/>
				<br><br>
			@endforeach
		@endforeach
		<button style="width:100%;" type="submit" class="btn btn-success">Saglabāt izmaiņas</button>
	</form>
	
	
	@foreach ($transp as $tr)
		<form id="transprts_{{ $tr->id }}" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="/admin/transports?id={{ $tr->id }}">
			<table style="margin-top: 32px;" class="transport">
				<tr>
					{!! csrf_field() !!}
					<td>
						<select name="type" form="transprts_{{ $tr->id }}">
							@if ($tr->type == 1)
								<option value="1" selected>autobuss</option>
								<option value="2">trolejbuss</option>
								<option value="3">tramvajs</option>
							@elseif ($tr->type == 2)
								<option value="1">autobuss</option>
								<option value="2" selected>trolejbuss</option>
								<option value="3">tramvajs</option>
							@else
								<option value="1">autobuss</option>
								<option value="2">trolejbuss</option>
								<option value="3" selected>tramvajs</option>
							@endif
						</select>
					</td>
					<td>
						<input type="text" style="text-align:center;" name="stop" form="transprts_{{ $tr->id }}" value="{{ $tr->stop }}" placeholder="Pieturas nosaukums"/>
					</td>
					<td>
						<input type="text" style="text-align:center;" name="numbers" form="transprts_{{ $tr->id }}" value="{{ $tr->numbers }}" placeholder="transporta numuri"/>
					</td>
					<td><button type="submit" form="transprts_{{ $tr->id }}" class="btn btn-success">Saglabāt izmaiņas</button></td>
					<td><a class="btn btn-danger delete" href="{{ url('/admin/transport/delete?id='. $tr->id) }}">Dzēst</a></td>
				</tr>
			</table>
		</form>
	@endforeach
	
	<form id="transprts_0" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="/admin/transports?id=0">
		<table style="margin-top: 32px;" class="transport">
			<tr>
				{!! csrf_field() !!}
				<td>
					<select name="type" form="transprts_0">
						<option value="1">autobuss</option>
						<option value="2">trolejbuss</option>
						<option value="3">tramvajs</option>
					</select>
				</td>
				<td>
					<input type="text" style="text-align:center;" name="stop" form="transprts_0" placeholder="Pieturas nosaukums"/>
				</td>
				<td>
					<input type="text" style="text-align:center;" name="numbers" form="transprts_0" placeholder="transporta numuri"/>
				</td>
				<td><button type="submit" form="transprts_0" class="btn btn-success">Pievienot</button></td>
			</tr>
		</table>
	</form>
</div>
@endsection
