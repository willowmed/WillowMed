@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>Labot pakalpojumu sarakstu, ko sniedz {{ $spec->name }} {{ $spec->surname }}</h2><br>
	<form id="user_profile" class="form-inline" enctype="multipart/form-data" role="form" method="POST" action="/admin/specialists/specialist/services/save?specId={{ $spec->id }}">
		{!! csrf_field() !!}
		@foreach ($serv as $S)
			<ul class="list-unstyled components">
				<li><!-- Link with dropdown items -->
					<a class="btn btn-default service-list-button" href="#{{ $S->id }}" data-toggle="collapse" aria-expanded="false">{{ $S->title }} <span class="caret"></span></a>
					<ul class="collapse list-unstyled" id="{{ $S->id }}">
						@foreach ($subServ as $subS)
							@if ($subS->serv_id == $S->id)
								<li>
									<?php $give_serv = 0 ?>
									@foreach ($specServ as $specS)
										@if ($specS->subserv_id == $subS->id)
											<?php $give_serv = 1 ?>
										@endif
									@endforeach
									@if ($give_serv == 1)
										<label style="margin-left: 20px; margin-rigt: 20px;"><input type="checkbox" name="spec_sub_serv[]" value="{{ $subS->id }}" checked> {{ $subS->title }}</label>
									@else
										<label style="margin-left: 20px; margin-rigt: 20px;"><input type="checkbox" name="spec_sub_serv[]" value="{{ $subS->id }}"> {{ $subS->title }}</label>
									@endif
								</li>
							@endif
						@endforeach
					</ul>
				</li>
			</ul>
		@endforeach
		<button style="width:100%;" type="submit" class="btn btn-success">Saglabāt izmaiņas</button>
	</form>
</div>
@endsection
