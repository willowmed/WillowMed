@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>Lietotāji</h2>
	
	@foreach ($users as $u)
		<form id="users" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="/admin/users/save?id={{ $u->id }}">
			{!! csrf_field() !!}
			<label>Lietotājvārds: <input type="text" style="text-align:center;" name="name" value="{{ $u->name }}" placeholder="Lietotājvārds"/></label>
			<label style="margin-left: 20px;">E-pasts: <input type="text" style="text-align:center;" name="email" value="{{ $u->email }}" placeholder="E-pasts"/></label>
			@if (Auth::user()->id != $u->id)
				@if ($u->super_user == 1)
					<label style="margin-left: 20px;"><input type="checkbox" name="super_user" checked> Admin</label>
				@else
					<label style="margin-left: 20px;"><input type="checkbox" name="super_user"> Admin</label>
				@endif
			@endif
			<button style="margin-left: 20px;" type="submit" class="btn btn-success">Saglabāt izmaiņas</button>
		</form>
		@if ($u->super_user != 1)
			<form id="user_delete" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="/admin/users/delete?id={{ $u->id }}">
				{!! csrf_field() !!}
				<button style="width: 100%; margin-top: 10px;" type="submit" class="btn btn-danger delete">Dzēst lietotāju: <b>"{{ $u->name }}"</b></button>
			</form>
		@endif
		<br><br><br>
	@endforeach
	<a style="width: 100%; margin-top: 50px;" class="btn btn-success" href="{{ route('register') }}">Reģistrēt jaunu lietotāju</a>
</div>
@endsection
