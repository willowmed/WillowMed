@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>
		@foreach ($menu as $menu_elem)
			@if (($menu_elem->name == $currentUrl) || ($menu_elem->name == ltrim($currentUrl, '/')))
				{{ $menu_elem->title }}
			@endif
		@endforeach
	</h2>
	<div class="container">
		<ul class="list-unstyled components">
			@foreach ($products as $p)
				<?php $serv_name = "" ?>
				@if ($p->serv_name != $serv_name)
					<?php $serv_name = $p->serv_name ?>
					<li  class="serv_list" style="margin-top: 5px;">
						<a href="#{{ $p->serv_id }}" data-toggle="collapse" aria-expanded="false">
							<div style="width: 100%;">
								{{ $p->serv_name }}<span class="caret"></span>
							</div>
						</a>
						<div class="collapse container spec-blocks" id="{{ $p->serv_id }}">
				@endif
							<a href="/admin/shop/product/edit?id={{$p->id}}">
								<div class="specialist">
									@if ($p->path=="")
										<img style="width:180px;" src="{{ asset('/images/profile/human.png') }}" alt="{{ $p->name }}"/>
									@else
										<img style="width:180px;" src="{{ asset($p->path) }}" alt="{{ $p->name }}"/>
									@endif
									<br><p>{{ $p->name }}</p>
								</div>
							</a>
				@if ($p->serv_name != $serv_name)
						</div>
					</li>
				@endif
			@endforeach
		</ul>
	</div>
	<div class="container">
		<br><a  class="btn btn-success" href="/admin/shop/product/edit?id=0" >Pievienot jaunu produktu</a>
	</div>
</div>
@endsection
