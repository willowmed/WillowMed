@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<!-- Include the TinyMCE script -->
	<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
	<script>
		tinymce.init({
		  selector: 'textarea',
		  height: 100,
		  plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code'
		  ],
		  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		  content_css: '//www.tinymce.com/css/codepen.min.css'
		});
	</script>
	<form id="home" class="form-inline" enctype="multipart/form-data" role="form" method="POST" action="/admin/home/save?pageId={{ $home->id }}">
		{!! csrf_field() !!}
		<label style="text-align:center;">Apraksts latviski: </label>
		<textarea id="textArea" style="width:100%; background-color: #333333;" name="content_lv" form="home">{{ $home->content_lv }}</textarea><br>
		<label style="text-align:center;">Apraksts angliski: </label>
		<textarea id="textArea" style="width:100%; background-color: #333333;" name="content_en" form="home">{{ $home->content_en }}</textarea><br>
		<label style="text-align:center;">Apraksts krieviski: </label>
		<textarea id="textArea" style="width:100%; background-color: #333333;" name="content_ru" form="home">{{ $home->content_ru }}</textarea><br><br>
		<button style="width:100%;" type="submit" class="btn btn-success">Saglabāt izmaiņas</button>
	</form>
</div>
@endsection
