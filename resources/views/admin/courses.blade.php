@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>Kuru saraksts</h2><br>
	<form id="courses" class="form-inline" enctype="multipart/form-data" role="form" method="POST" action="/admin/courses/save">
		{!! csrf_field() !!}
		@foreach ($courses as $C)
			<ul class="list-unstyled components">
				<li>
					@if ($C->hide == 0)
						<label style="margin-right: 20px;"><input type="checkbox" name="hide_course[]" value="{{ $C->id }}" checked/> Rādīt</label>
					@elseif ($C->hide == 1)
						<label style="margin-right: 20px;"><input type="checkbox" name="hide_course[]" value="{{ $C->id }}"/> Rādīt</label>
					@endif
					<a href="/admin/courses/course/edit?courseId={{ $C->id }}" class="price_l">{{ $C->name }} - Labot</a>
				</li>
			</ul>
		@endforeach
		<button style="width:100%; margin-top: 20px;" type="submit" class="btn btn-success">Saglabāt izmaiņas</button>
	</form>
	<br>
	<a href="/admin/courses/course/edit?courseId=0" style="width:100%;" class="btn btn-success">Pievienot kursus</a>
</div>
@endsection
