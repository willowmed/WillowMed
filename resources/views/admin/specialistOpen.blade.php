@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	@if ($id == 0)
		<h2>Izveidot profilu</h2><br>
	@else
		<h2>Labot profilu</h2><br>
	@endif
	<a href="/admin/specialists/specialist/services/edit?specId={{ $spec->id }}"  class="btn btn-default">Sniegto pakalpojumu saraksts</a><br><br>
	<!-- Include the TinyMCE script -->
	<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
	<script>
		tinymce.init({
		  selector: 'textarea',
		  height: 75,
		  plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code'
		  ],
		  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		  content_css: '//www.tinymce.com/css/codepen.min.css'
		});
	</script>
	<form id="user_profile" class="form-inline" enctype="multipart/form-data" role="form" method="POST" action="/admin/specialists/specialist/save?id={{ $spec->id }}">
		{!! csrf_field() !!}
		@if ($spec->path=="")
			<label style="text-align:center;">Izvēlēties attēlu(.png/.jpg/.jpeg):</label>	
			<input style="text-align:center;" type="file" name="picture" form="user_profile"></input><br>
		@else
			<img style="width:180px;" src="{{ asset($spec->path) }}" alt="{{ $spec->name }}"/>
			<a href="/admin/specialists/specialist/delete_img?id={{ $spec->id }}" class="btn btn-danger delete">Dzēst attēlu</a><br><br>
		@endif

		<label style="text-align:center; margin-right: 20px;">Vārds: 
		<input type="text" style="text-align:center;" name="name" form="user_profile" value="{{ $spec->name }}" placeholder="Vārds"/></label>
		<label style="text-align:center; margin-right: 20px;">Uzvārds: 
		<input type="text" style="text-align:center;" name="surname" form="user_profile" value="{{ $spec->surname }}" placeholder="Uzvārds"/></label>
		<label style="text-align:center; margin-right: 20px;">Profesija latviski: 
		<input type="text" style="text-align:center;" name="prof_lv" form="user_profile" value="{{ $spec->prof_lv }}" placeholder="Profesija latviski"/></label>
		<label style="text-align:center; margin-right: 20px;">Profesija angliski: 
		<input type="text" style="text-align:center;" name="prof_en" form="user_profile" value="{{ $spec->prof_en }}" placeholder="Profesija angliski"/></label>
		<label style="text-align:center; margin-right: 20px;">Profesija krieviski: 
		<input type="text" style="text-align:center;" name="prof_ru" form="user_profile" value="{{ $spec->prof_ru }}" placeholder="Profesija krieviski"/></label>
		@if ($spec->hide == 1)
			<label style="margin-right: 20px;"><input type="checkbox" name="hide" name="1" checked> Paslēpt</label>
		@else
			<label style="margin-right: 20px;"><input type="checkbox" name="hide" name="1"> Paslēpt</label>
		@endif
		<br><br>
		<?php 
			$is_def = 0;
		?>
		@foreach ($spec_section as $sect)
			@if ($sect->subs == "what_is")
				<?php $is_def = 1; ?>
				<label style="text-align:center;">Kas ir latviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="what_is_lv" form="user_profile">{{ $sect->description_lv }}</textarea><br>
				<label style="text-align:center;">Kas ir angliski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="what_is_en" form="user_profile">{{ $sect->description_en }}</textarea><br>
				<label style="text-align:center;">Kas ir krieviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="what_is_ru" form="user_profile">{{ $sect->description_ru }}</textarea><br>
			@endif
		@endforeach
		@if ($is_def == 0)
			<?php $is_def = 1; ?>
			<label style="text-align:center;">Kas ir latviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="what_is_lv" form="user_profile"></textarea><br>
			<label style="text-align:center;">Kas ir angliski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="what_is_en" form="user_profile"></textarea><br>
			<label style="text-align:center;">Kas ir krieviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="what_is_ru" form="user_profile"></textarea><br>
		@endif
		@foreach ($spec_section as $sect)
			@if ($sect->subs == "about_me")
				<?php $is_def = 0; ?>
				<label style="text-align:center;">Par mani latviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="about_me_lv" form="user_profile">{{ $sect->description_lv }}</textarea><br>
				<label style="text-align:center;">Par mani angliski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="about_me_en" form="user_profile">{{ $sect->description_en }}</textarea><br>
				<label style="text-align:center;">Par mani krieviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="about_me_ru" form="user_profile">{{ $sect->description_ru }}</textarea><br>
			@endif
		@endforeach
		@if ($is_def == 1)
			<?php $is_def = 0; ?>
			<label style="text-align:center;">Par mani latviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="about_me_lv" form="user_profile"></textarea><br>
			<label style="text-align:center;">Par mani angliski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="about_me_en" form="user_profile"></textarea><br>
			<label style="text-align:center;">Par mani krieviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="about_me_ru" form="user_profile"></textarea><br>
		@endif
		@foreach ($spec_section as $sect)
			@if ($sect->subs == "education")
				<?php $is_def = 1; ?>
				<label style="text-align:center;">Izglītība latviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="education_lv" form="user_profile">{{ $sect->description_lv }}</textarea><br>
				<label style="text-align:center;">Izglītība angliski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="education_en" form="user_profile">{{ $sect->description_en }}</textarea><br>
				<label style="text-align:center;">Izglītība krieviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="education_ru" form="user_profile">{{ $sect->description_ru }}</textarea><br>
			@endif
		@endforeach
		@if ($is_def == 0)
			<?php $is_def = 1; ?>
			<label style="text-align:center;">Izglītība latviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="education_lv" form="user_profile"></textarea><br>
			<label style="text-align:center;">Izglītība angliski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="education_en" form="user_profile"></textarea><br>
			<label style="text-align:center;">Izglītība krieviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="education_ru" form="user_profile"></textarea><br>
		@endif
		@foreach ($spec_section as $sect)
			@if ($sect->subs == "work_history")
				<?php $is_def = 0; ?>
				<label style="text-align:center;">Profesionālā darbība latviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="work_history_lv" form="user_profile">{{ $sect->description_lv }}</textarea><br>
				<label style="text-align:center;">Profesionālā darbība angliski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="work_history_en" form="user_profile">{{ $sect->description_en }}</textarea><br>
				<label style="text-align:center;">Profesionālā darbība krieviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="work_history_ru" form="user_profile">{{ $sect->description_ru }}</textarea><br>
			@endif
		@endforeach
		@if ($is_def == 1)
			<?php $is_def = 0; ?>
			<label style="text-align:center;">Profesionālā darbība latviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="work_history_lv" form="user_profile"></textarea><br>
			<label style="text-align:center;">Profesionālā darbība angliski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="work_history_en" form="user_profile"></textarea><br>
			<label style="text-align:center;">Profesionālā darbība krieviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="work_history_ru" form="user_profile"></textarea><br>
		@endif
		
		<button style="width:100%;" type="submit" class="btn btn-success">Saglabāt izmaiņas</button>
	</form>
</div>
@endsection
