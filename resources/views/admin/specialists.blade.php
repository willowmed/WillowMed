@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>
		@foreach ($menu as $menu_elem)
			@if (($menu_elem->name == $currentUrl) || ($menu_elem->name == ltrim($currentUrl, '/')))
				{{ $menu_elem->title }}
			@endif
		@endforeach
	</h2>
	<div class="container spec-blocks">
		@foreach ($specialists as $spec)
			<a class="" href="/admin/specialists/specialist/edit?id={{ $spec->id }}" >
				<div class="specialist">
					@if ($spec->path != "")
						<img class="spec-img" src="{{ asset($spec->path) }}" alt="{{ $spec->name }} {{ $spec->surname }}"/>
					@else
						<img class="spec-img" src="{{ asset('/images/profile/human.png') }}" alt="{{ $spec->name }} {{ $spec->surname }}"/>
					@endif
					<p><b>{{ $spec->name }} {{ $spec->surname }}</b></p>
				</div>
			</a>
		@endforeach
	</div>
	<div class="container">
		<br><a  class="btn btn-success" href="/admin/specialists/specialist/edit?id=0" >Pievienot jaunu</a>
	</div>
</div>
@endsection
