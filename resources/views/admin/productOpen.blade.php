@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	@if ($id == 0)
		<h2>Pievienot produktu</h2><br>
	@else
		<h2>Labot produktu</h2><br>
	@endif
	<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
	<script>
		tinymce.init({
		  selector: 'textarea',
		  height: 100,
		  plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code'
		  ],
		  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		  content_css: '//www.tinymce.com/css/codepen.min.css'
		});
	</script>
	<form id="product" class="form-inline" enctype="multipart/form-data" role="form" method="POST" action="/admin/shop/product/save?id={{ $product->id }}">
		{!! csrf_field() !!}
		@if ($product->path=="")
			<label style="text-align:center;">Izvēlēties attēlu(.png/.jpg/.jpeg):</label>	
			<input style="text-align:center;" type="file" name="picture" form="product"></input><br>
		@else
			<img style="width:180px;" src="{{ asset($product->path) }}" alt="{{ $product->name_lv }}"/>
			<a href="/admin/shop/product/delete_img?id={{ $product->id }}" class="btn btn-danger delete">Dzēst attēlu</a><br><br>
		@endif

		<label style="text-align:center;">Nosaukums latviski: 
			<input type="text" style="text-align:center;" name="name_lv" form="product" value="{{ $product->name_lv }}" placeholder="Nosaukums latviski"/>
		</label>
		<label style="text-align:center;">Nosaukums angliski: 
			<input type="text" style="text-align:center;" name="name_en" form="product" value="{{ $product->name_en }}" placeholder="Nosaukums angliski"/>
		</label>
		<label style="text-align:center;">Nosaukums krieviski: 
			<input type="text" style="text-align:center;" name="name_ru" form="product" value="{{ $product->name_ru }}" placeholder="Nosaukums krieviski"/>
		</label>
		<label style="text-align:center;">Cena: 
			<input type="number" min="0.00" step="0.01"  style="text-align:center; font-weight: normal;" name="price" form="product" value="{{ $product->price}}" placeholder="Cena"/>
		</label>
		@if ($product->hide == 1)
			<label style="margin-left: 20px;"><input type="checkbox" name="hide" checked> Paslēpt</label>
		@else
			<label style="margin-left: 20px;"><input type="checkbox" name="hide"> Paslēpt</label>
		@endif
		<label style="width:100%;">Pakalpojuma kategorija:
			<select name="serv_id">
				@foreach ($services as $S)
					@if ($product->serv_id == $S->id)
						<option value="{{ $S->id }}" selected>{{ $S->title }}</option>
					@else
						<option value="{{ $S->id }}">{{ $S->title }}</option>
					@endif
				@endforeach
			</select>
		</label>
		<br><br>
		<label style="text-align:center;">Apraksts latviski: </label>
		<textarea id="textArea" style="width:100%; background-color: #333333;" name="description_lv" form="product">{{ $product->description_lv }}</textarea><br>
		<label style="text-align:center;">Apraksts angliski: </label>
		<textarea id="textArea" style="width:100%; background-color: #333333;" name="description_en" form="product">{{ $product->description_en }}</textarea><br>
		<label style="text-align:center;">Apraksts krieviski: </label>
		<textarea id="textArea" style="width:100%; background-color: #333333;" name="description_ru" form="product">{{ $product->description_ru }}</textarea><br>
		
		<button style="width:100%;" type="submit" class="btn btn-success">Saglabāt izmaiņas</button>
	</form>
</div>
@endsection
