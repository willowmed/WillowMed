@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<h2>Labot pakalpojumu sarakstu</h2><br>
	<form id="user_profile" class="form-inline" enctype="multipart/form-data" role="form" method="POST" action="/admin/services/save">
		{!! csrf_field() !!}
		@foreach ($serv as $S)
			<ul class="list-unstyled components">
				<li><!-- Link with dropdown items -->
					<a href="/admin/services/service/edit?servId={{ $S->id }}" class="btn btn-default service-list-button">{{ $S->title }} - Labot</a>
					<a class="btn btn-default" href="#{{ $S->id }}" data-toggle="collapse" aria-expanded="false"><span class="caret"></span></a>
					@if ($S->hide == 1)
						<label style="margin-left: 20px;"><input type="checkbox" name="hide_serv[]" value="{{ $S->id }}"/> Rādīt kategoriju </label>
					@else
						<label style="margin-left: 20px;"><input type="checkbox" name="hide_serv[]" value="{{ $S->id }}" checked/> Rādīt kategoriju </label>
					@endif
					<ul class="collapse list-unstyled" id="{{ $S->id }}">
						@foreach ($subServ as $subS)
							@if ($subS->serv_id == $S->id && $subS->hide == 1)
								<li><input style="margin-left: 20px;" type="checkbox" name="hide_sub_serv[]" value="{{ $subS->id }}"/> {{ $subS->title }} </li>
							@elseif ($subS->serv_id == $S->id && $subS->hide != 1)
								<li><input style="margin-left: 20px;" type="checkbox" name="hide_sub_serv[]" value="{{ $subS->id }}" checked> {{ $subS->title }} </li>
							@endif
						@endforeach
					</ul>
				</li>
			</ul>
		@endforeach
		<button style="width:100%; margin-top: 20px;" type="submit" class="btn btn-success">Saglabāt izmaiņas</button>
	</form>
	<br>
	<a href="/admin/services/service/edit?servId=0" style="width:100%;" class="btn btn-success">Pievienot jaunu kategoriju</a>
</div>
@endsection
