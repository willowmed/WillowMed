@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	<!-- Include the TinyMCE script -->
	<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
	<script>
		tinymce.init({
		  selector: 'textarea',
		  height: 100,
		  plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code'
		  ],
		  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		  content_css: '//www.tinymce.com/css/codepen.min.css'
		});
	</script>
	<form id="about" class="form-inline" enctype="multipart/form-data" role="form" method="POST" action="/admin/about-us/save?pageId={{ $about->id }}">
		{!! csrf_field() !!}
		<label style="text-align:center;">Apraksts latviski: </label>
		<textarea id="textArea" style="width:100%; background-color: #333333;" name="content_lv" form="about">{{ $about->content_lv }}</textarea><br>
		<label style="text-align:center;">Apraksts angliski: </label>
		<textarea id="textArea" style="width:100%; background-color: #333333;" name="content_en" form="about">{{ $about->content_en }}</textarea><br>
		<label style="text-align:center;">Apraksts krieviski: </label>
		<textarea id="textArea" style="width:100%; background-color: #333333;" name="content_ru" form="about">{{ $about->content_ru }}</textarea><br><br>
		<?php 
			$is_def = 0;
		?>
		@foreach ($spec_section as $sect)
			@if ($sect->subs == "collective_goal")
				<?php $is_def = 1; ?>
				<label style="text-align:center;">Mērķi latviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="collective_goal_lv" form="about">{{ $sect->description_lv }}</textarea><br>
				<label style="text-align:center;">Mērķi angliski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="collective_goal_en" form="about">{{ $sect->description_en }}</textarea><br>
				<label style="text-align:center;">Mērķi krieviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="collective_goal_ru" form="about">{{ $sect->description_ru }}</textarea><br>
			@endif
		@endforeach
		@if ($is_def == 0)
			<?php $is_def = 1; ?>
			<label style="text-align:center;">Mērķi latviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="collective_goal_lv" form="about"></textarea><br>
			<label style="text-align:center;">Mērķi angliski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="collective_goal_en" form="about"></textarea><br>
			<label style="text-align:center;">Mērķi krieviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="collective_goal_ru" form="about"></textarea><br>
		@endif
		@foreach ($spec_section as $sect)
			@if ($sect->subs == "collective_vision")
				<?php $is_def = 0; ?>
				<label style="text-align:center;">Vīzija latviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="collective_vision_lv" form="about">{{ $sect->description_lv }}</textarea><br>
				<label style="text-align:center;">Vīzija angliski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="collective_vision_en" form="about">{{ $sect->description_en }}</textarea><br>
				<label style="text-align:center;">Vīzija krieviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="collective_vision_ru" form="about">{{ $sect->description_ru }}</textarea><br>
			@endif
		@endforeach
		@if ($is_def == 1)
			<?php $is_def = 0; ?>
			<label style="text-align:center;">Vīzija latviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="collective_vision_lv" form="about"></textarea><br>
			<label style="text-align:center;">Vīzija angliski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="collective_vision_en" form="about"></textarea><br>
			<label style="text-align:center;">Vīzija krieviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="collective_vision_ru" form="about"></textarea><br>
		@endif
		@foreach ($spec_section as $sect)
			@if ($sect->subs == "believes")
				<?php $is_def = 1; ?>
				<label style="text-align:center;">Ticam latviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="believes_lv" form="about">{{ $sect->description_lv }}</textarea><br>
				<label style="text-align:center;">Ticam angliski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="believes_en" form="about">{{ $sect->description_en }}</textarea><br>
				<label style="text-align:center;">Ticam krieviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="believes_ru" form="about">{{ $sect->description_ru }}</textarea><br>
			@endif
		@endforeach
		@if ($is_def == 0)
			<?php $is_def = 1; ?>
			<label style="text-align:center;">Ticam latviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="believes_lv" form="about"></textarea><br>
			<label style="text-align:center;">Ticam angliski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="believes_en" form="about"></textarea><br>
			<label style="text-align:center;">Ticam krieviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="believes_ru" form="about"></textarea><br>
		@endif
		@foreach ($spec_section as $sect)
			@if ($sect->subs == "will_do")
				<?php $is_def = 0; ?>
				<label style="text-align:center;">Darīsim latviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="will_do_lv" form="about">{{ $sect->description_lv }}</textarea><br>
				<label style="text-align:center;">Darīsim angliski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="will_do_en" form="about">{{ $sect->description_en }}</textarea><br>
				<label style="text-align:center;">Darīsim krieviski: </label>
				<textarea id="textArea" style="width:100%; background-color: #333333;" name="will_do_ru" form="about">{{ $sect->description_ru }}</textarea><br>
			@endif
		@endforeach
		@if ($is_def == 1)
			<?php $is_def = 0; ?>
			<label style="text-align:center;">Darīsim latviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="will_do_lv" form="about"></textarea><br>
			<label style="text-align:center;">Darīsim angliski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="will_do_en" form="about"></textarea><br>
			<label style="text-align:center;">Darīsim krieviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="will_do_ru" form="about"></textarea><br>
		@endif
		<button style="width:100%;" type="submit" class="btn btn-success">Saglabāt izmaiņas</button>
	</form>
</div>
@endsection
