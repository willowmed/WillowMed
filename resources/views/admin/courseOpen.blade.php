@extends('layouts.layout')

@section('content')
<div class="container {{ $currentUrl }}">
	@if ($id == 0)
		<h2>Pievienot kursu</h2><br>
	@else
		<h2>Labot kursu</h2><br>
	@endif
	<!-- Include the TinyMCE script -->
	<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
	<script>
		tinymce.init({
		  selector: 'textarea',
		  height: 100,
		  plugins: [
			'advlist autolink lists link image charmap print preview anchor',
			'searchreplace visualblocks code fullscreen',
			'insertdatetime media table contextmenu paste code'
		  ],
		  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		  content_css: '//www.tinymce.com/css/codepen.min.css'
		});
	</script>
	<form id="sub_serv" class="form-inline" enctype="multipart/form-data" role="form" method="POST" action="/admin/courses/course/save?courseId={{ $subServ->id }}">
		{!! csrf_field() !!}
		<label style="text-align:center; margin-right: 20px;">Noskaums latviski: 
		<input type="text" style="width: 500px; text-align:center; font-weight: normal;" name="name_lv" form="sub_serv" value="{{ $subServ->name_lv}}" placeholder="Noskaums latviski"/></label>
		<label style="text-align:center; margin-right: 20px;">Noskaums angliski: 
		<input type="text" style="width: 500px; text-align:center; font-weight: normal;" name="name_en" form="sub_serv" value="{{ $subServ->name_en}}" placeholder="Noskaums angliski"/></label>
		<label style="text-align:center; margin-right: 20px;">Noskaums krieviski: 
		<input type="text" style="width: 500px; text-align:center; font-weight: normal;" name="name_ru" form="sub_serv" value="{{ $subServ->name_ru}}" placeholder="Noskaums krieviski"/></label>
		<br>
		@if ($subServ->min_price == 0)
			<label style="margin-right: 20px;"><input type="checkbox" name="min_price" value="{{ $subServ->id }}"/> Cena sākot no: 
		@else
			<label style="margin-right: 20px;"><input type="checkbox" name="min_price" value="{{ $subServ->id }}" checked/> Cena sākot no: 
		@endif
		<input type="number" min="0.00" step="0.01"  style="text-align:center; font-weight: normal;" name="price" form="sub_serv" value="{{ $subServ->price}}" placeholder="Cena"/></label>
		<label style="text-align:center; margin-right: 20px;">Lekcijas ilgums: 
		<input type="text" style="text-align:center; font-weight: normal;" name="duration" form="sub_serv" value="{{ $subServ->duration}}" placeholder="Lekcijas ilgums"/> (min)</label>
		@if ($subServ->hide == 1)
			<label style="margin-left: 20px;"><input type="checkbox" name="hide_sub_serv" value="{{ $subServ->id }}"/> Rādīt pakalpojumu </label>
		@else
			<label style="margin-left: 20px;"><input type="checkbox" name="hide_sub_serv" value="{{ $subServ->id }}" checked/> Rādīt pakalpojumu </label>
		@endif
		<div class="form-group">
			<label>Speciālistu saraksts: </label>
			<select id="framework" name="serv_spec[]" multiple class="form-control" >
				@foreach ($spec as $S)
					<?php $give_serv = 0; ?>
					@foreach ($serv_spec as $sepc_s)
						@if ($S->id == $sepc_s->spec_id && $subServ->id > 0)
							<?php $give_serv = 1; ?>
						@endif
					@endforeach
					@if ($give_serv == 1)
						<option value="{{ $S->id }}" selected>{{ $S->name }} {{ $S->surname }} ({{ $S->prof }})</option>
					@else
						<option value="{{ $S->id }}">{{ $S->name }} {{ $S->surname }} ({{ $S->prof }})</option>
					@endif
				@endforeach
			</select>
		</div>
		<br>
		<label style="text-align:center;">Apraksts latviski: </label>
		<textarea id="textArea" style="width:100%; background-color: #333333;" name="description_lv" form="sub_serv" placeholder="Apraksts latviski">{{ $subServ->description_lv }}</textarea><br>
		<label style="text-align:center;">Apraksts angliski: </label>
		<textarea id="textArea" style="width:100%; background-color: #333333;" name="description_en" form="sub_serv" placeholder="Apraksts angliski">{{ $subServ->description_en }}</textarea><br>
		<label style="text-align:center;">Apraksts krieviski: </label>
		<textarea id="textArea" style="width:100%; background-color: #333333;" name="description_ru" form="sub_serv" placeholder="Apraksts krieviski">{{ $subServ->description_ru }}</textarea><br>
		
		<button style="width:100%; margin-top: 20px;" type="submit" class="btn btn-success">Saglabāt izmaiņas</button>
	</form>
	<script>
		$(document).ready(function(){
			$('#framework').multiselect({
				nonSelectedText: 'Speciālisi',
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				buttonWidth:'400px'
			});
		});
	</script>
</div>
@endsection
