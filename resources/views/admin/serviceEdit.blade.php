@extends('layouts.layout')

@section('content')
<div class="container">
	<h2>Labot pakalpojuma kategoriju</h2>
	<div class="container">
		<!-- Include the TinyMCE script -->
		<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
		<script>
			tinymce.init({
			  selector: 'textarea',
			  height: 100,
			  plugins: [
				'advlist autolink lists link image charmap print preview anchor',
				'searchreplace visualblocks code fullscreen',
				'insertdatetime media table contextmenu paste code'
			  ],
			  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
			  content_css: '//www.tinymce.com/css/codepen.min.css'
			});
		</script>
		<form id="service" class="form-inline" enctype="multipart/form-data" role="form" method="POST" action="/admin/services/service/save?servId={{ $serv->id }}">
			{!! csrf_field() !!}
			<label style="text-align:center; margin-right: 20px;">Noskaums latviski: 
			<input type="text" style="text-align:center; font-weight: normal;" name="name_lv" form="service" value="{{ $serv->name_lv}}" placeholder="Noskaums latviski"/></label>
			<label style="text-align:center; margin-right: 20px;">Noskaums angliski: 
			<input type="text" style="text-align:center; font-weight: normal;" name="name_en" form="service" value="{{ $serv->name_en}}" placeholder="Noskaums angliski"/></label>
			<label style="text-align:center;">Noskaums krieviski: 
			<input type="text" style="text-align:center; font-weight: normal;" name="name_ru" form="service" value="{{ $serv->name_ru}}" placeholder="Noskaums krieviski"/></label>
			@if ($serv->hide == 1)
				<label style="margin-left: 20px;"><input type="checkbox" name="hide_serv" value="{{ $serv->id }}"/> Rādīt kategoriju </label>
			@else
				<label style="margin-left: 20px;"><input type="checkbox" name="hide_serv" value="{{ $serv->id }}" checked/> Rādīt kategoriju </label>
			@endif
			<br>
			<label style="text-align:center;">Apraksts latviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="description_lv" form="service" placeholder="Apraksts latviski">{{ $serv->description_lv }}</textarea><br>
			<label style="text-align:center;">Apraksts angliski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="description_en" form="service" placeholder="Apraksts angliski">{{ $serv->description_en }}</textarea><br>
			<label style="text-align:center;">Apraksts krieviski: </label>
			<textarea id="textArea" style="width:100%; background-color: #333333;" name="description_ru" form="service" placeholder="Apraksts krieviski">{{ $serv->description_ru }}</textarea><br>
		
			<br><br><label style="width:100%;">Pakalpojumu saraksts: </label><br><br>
			@foreach ($subServ as $S)
				<label style="width:100%;">@if ($S->hide == 1)
					<input type="checkbox" name="hide_sub_serv[]" value="{{ $S->id }}"/>
				@else
					<input type="checkbox" name="hide_sub_serv[]" value="{{ $S->id }}" checked/>
				@endif
				<a href="/admin/services/subservice/edit?subServId={{ $S->id }}"  class="btn btn-default service-list-button">{{ $S->title }} - Labot</a>
				<a href="/admin/services/subservice/delete?subServId={{ $S->id }}"  class="btn btn-danger delete"> Dzēst </a>
				</label>
			@endforeach
			<br><br>
			<button style="width:100%; margin-top: 10px;" type="submit" class="btn btn-success">Saglabāt izmaiņas</button>
		</form>
		@if ($serv->id >0)
			<br>
			<a href="/admin/services/subservice/edit?subServId=0" style="width:100%;" class="btn btn-success">Pievienot jaunu pakalpojumu</a>
			<br><br>
			<br><br>
			<a href="/admin/services/service/delete?servId={{ $serv->id }}" style="width:100%;" class="btn btn-danger delete">Dzēst kategoriju ar pakalpojumiem</a>
		@endif
	</div>
</div>
@endsection
