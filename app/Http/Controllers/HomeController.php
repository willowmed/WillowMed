<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use DB;
use Schema;
use URL;

class HomeController extends Controller
{
    private $language = 'lv';
	private $lvUrl = '';
	private $enUrl = '';
	private $ruUrl = '';
	private $currentUrl = '';
	
    public function __construct() {
        //$this->middleware('auth');
		$this->currentUrl = $_SERVER['REQUEST_URI'];
        $this->currentUrl = explode('?', $this->currentUrl)[0];
		
		// Get Language
		$this->language = explode('/', $this->currentUrl)[1];
		if ($this->language != 'en' && $this->language != 'ru') {
			$this->language = 'lv';
		} 
		// Get main menu
        $this->menu = DB::select("SELECT *, name_{$this->language} as title FROM menu ORDER BY ord");
		HomeController::transl();
    }
	
	public function index() {
		HomeController::langURL();
		HomeController::transl();
		// Active menu element
		$this->currentUrl = $_SERVER['REQUEST_URI'];
		$this->currentUrl = explode('?', $this->currentUrl)[0];	
		$array = explode('/', $this->currentUrl);
		if(isset($array[2])) $this->currentUrl = $array[2]; else $this->currentUrl = 'home';
		$home = DB::select("SELECT content_{$this->language} as content FROM pages WHERE name = '{$this->currentUrl}'")[0];
		
		return view('home', [
			'menu' => $this->menu,
			'lvUrl' => $this->lvUrl,
			'enUrl' => $this->enUrl,
			'ruUrl' => $this->ruUrl,
			'lang' => $this->language,
			'footer' => $this->footerTransl,
			'home' => $home,
			'currentUrl' => $this->currentUrl,
		]);
	}
	
	public function about() {
		HomeController::langURL();
		HomeController::transl();
		// Active menu element
		$this->currentUrl = $_SERVER['REQUEST_URI'];
		$this->currentUrl = explode('?', $this->currentUrl)[0];
		$array = explode('/', $this->currentUrl);
		if(isset($array[2])) $this->currentUrl = $array[2]; else $this->currentUrl = 'about-us';
		$about = DB::select("SELECT content_{$this->language} as content FROM pages WHERE name = '{$this->currentUrl}'")[0];
		$spec_section = DB::select("SELECT section.id as sec_id, section.name_{$this->language} as name, section.subsection as subs, spec_info.description_{$this->language} as info 
				FROM section INNER JOIN spec_info ON section.id = spec_info.section_id 
				WHERE section.section = 'about'");
		
		return view('about', [
			'menu' => $this->menu,
			'lvUrl' => $this->lvUrl,
			'enUrl' => $this->enUrl,
			'ruUrl' => $this->ruUrl,
			'lang' => $this->language,
			'footer' => $this->footerTransl,
			'about' => $about,
			'spec_section' => $spec_section,
			'currentUrl' => $this->currentUrl,
		]);
	}
	
	public function services() {
		HomeController::langURL();
		HomeController::transl();
		// Active menu element
		$this->currentUrl = $_SERVER['REQUEST_URI'];
		$this->currentUrl = explode('?', $this->currentUrl)[0];
		$array = explode('/', $this->currentUrl);
		if(isset($array[2])) $this->currentUrl = $array[2]; else $this->currentUrl = 'services';
		$serv = DB::select("SELECT services.id as servId, services.name_{$this->language} as name, subservice.id as subServId, subservice.name_{$this->language} as subName 
			FROM services INNER JOIN subservice ON services.id = subservice.serv_id WHERE services.hide = 0 AND subservice.hide = 0 AND subservice.course = 0 
			ORDER BY services.name_{$this->language} ASC, subservice.name_{$this->language} ASC");
		
		return view('services', [
			'menu' => $this->menu,
			'lvUrl' => $this->lvUrl,
			'enUrl' => $this->enUrl,
			'ruUrl' => $this->ruUrl,
			'lang' => $this->language,
			'footer' => $this->footerTransl,
			'services' => $serv,
			'currentUrl' => $this->currentUrl,
		]);
	}
	public function serviceList() {
		HomeController::langURL();
		HomeController::transl();
		// Active menu element
		$this->currentUrl = $_SERVER['REQUEST_URI'];
		$this->currentUrl = explode('?', $this->currentUrl)[0];
		$array = explode('/', $this->currentUrl);
		if(isset($array[2])) $this->currentUrl = $array[2]; else $this->currentUrl = 'services';
		if (isset($_GET['serviceId']) && ($serviceId = intval($_GET['serviceId'])) > 0) {
			$s = DB::select("SELECT id FROM services");
			$exists = 0;
			foreach ($s as $se) {
				if ($se->id == $serviceId) {
					$exists = 1;
				}
			}
			if ($exists == 0) {
				return redirect("/{$this->language}/services");
			}
			$service = DB::select("SELECT *, name_{$this->language} as title, description_{$this->language} as description FROM services WHERE id = {$serviceId}")[0];
			$subservices = DB::select("SELECT *, name_{$this->language} as title FROM subservice WHERE serv_id = {$serviceId} AND hide = 0 AND course = 0 ORDER BY name_{$this->language} ASC");
			
			return view('serviceList', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'transl' => $this->translVal,
				'service' => $service,
				'subserv' => $subservices,
				'currentUrl' => $this->currentUrl,
			]);
		}
		if (isset($_GET['subServiceId']) && ($subServiceId = intval($_GET['subServiceId'])) > 0) {
			$s = DB::select("SELECT id, course, hide FROM subservice");
			$exists = 0;
			foreach ($s as $se) {
				if ($se->id == $subServiceId && $se->course == 0 && $se->hide == 0) {
					$exists = 1;
				}
			}
			if ($exists == 0) {
				return redirect("/{$this->language}/services");
			}
			$price = "";
			foreach ($this->translVal as $tr) {
				if ($tr->infoKey == "price") $price = $tr->infoVal;
			}
			$from = "";
			foreach ($this->translVal as $tr) {
				if ($tr->infoKey == "from") $from = $tr->infoVal;
			}
			$dur = "";
			foreach ($this->translVal as $tr) {
				if ($tr->infoKey == "duration") $dur = $tr->infoVal;
			}
			$agreem = "";
			foreach ($this->translVal as $tr) {
				if ($tr->infoKey == "by_agreement") $agreem = $tr->infoVal;
			}
			$subservice = DB::select("SELECT *, name_{$this->language} as title, description_{$this->language} as info, price, duration FROM subservice WHERE id = {$subServiceId}")[0];
			$service = DB::select("SELECT id, name_{$this->language} as title FROM services WHERE id = {$subservice->serv_id}")[0];
			$specialists = DB::select("SELECT servis_spec.subserv_id as subServId, 
						specialists.name as name, specialists.surname as surname, specialists.id as sepcId, 
						specialists.prof_{$this->language} as prof 
						FROM specialists INNER JOIN servis_spec ON servis_spec.spec_id = specialists.id 
						WHERE servis_spec.subserv_id = {$subServiceId} AND specialists.hide = 0 ORDER BY specialists.surname ASC, specialists.name ASC");
			$spec = "";
			$ar = array();
			foreach ($specialists as $i) {
				array_push($ar, "<a href=\"/{$this->language}/specialists/specialist?id={$i->sepcId}\">{$i->name} {$i->surname} (<i class=\"prof_s\">{$i->prof}</i>)</a>");
			}
			$spec = implode(", ", $ar);
			
			return view('subService', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'transl' => $this->translVal,
				'price' => $price,
				'from' => $from,
				'dur' => $dur,
				'agreem' => $agreem,
				'service' => $service,
				'spec' => $spec,
				'subserv' => $subservice,
				'currentUrl' => $this->currentUrl,
			]);
		}
	}
	
	public function pricelist() {
		HomeController::langURL();
		HomeController::transl();
		// Active menu element
		$this->currentUrl = $_SERVER['REQUEST_URI'];
		$this->currentUrl = explode('?', $this->currentUrl)[0];
		$array = explode('/', $this->currentUrl);
		if(isset($array[2])) $this->currentUrl = $array[2]; else $this->currentUrl = 'priceplist';
		$serv = DB::select("SELECT subservice.id as subId, subservice.name_{$this->language} as subName, subservice.serv_id as servId, subservice.course as courses,
			subservice.price as price, services.name_{$this->language} as servName, subservice.duration as duration, subservice.min_price as min_price 
			FROM subservice INNER JOIN services ON subservice.serv_id = services.id 
			WHERE subservice.hide = 0 AND services.hide = 0 AND subservice.course = 0 ORDER BY services.name_{$this->language} ASC, subservice.name_{$this->language} ASC");
		
		$courses = DB::select("SELECT subservice.id as subId, subservice.name_{$this->language} as subName, subservice.serv_id as servId, 
			subservice.price as price, subservice.duration as duration, subservice.min_price as min_price 
			FROM subservice 
			WHERE subservice.hide = 0 AND subservice.course = 1 ORDER BY subservice.name_{$this->language} ASC");
		$from = "";
		foreach ($this->translVal as $tr) {
			if ($tr->infoKey == "from") $from = $tr->infoVal;
		}
		$agreem = "";
		foreach ($this->translVal as $tr) {
			if ($tr->infoKey == "by_agreement") $agreem = $tr->infoVal;
		}
		
		return view('pricelist', [
			'menu' => $this->menu,
			'lvUrl' => $this->lvUrl,
			'enUrl' => $this->enUrl,
			'ruUrl' => $this->ruUrl,
			'lang' => $this->language,
			'footer' => $this->footerTransl,
			'transl' => $from,
			'agreem' => $agreem,
			'services' => $serv,
			'courses' => $courses,
			'currentUrl' => $this->currentUrl,
		]);
	}
	
	public function specialists() {
		HomeController::langURL();
		HomeController::transl();
		// Active menu element
		$this->currentUrl = $_SERVER['REQUEST_URI'];
		$this->currentUrl = explode('?', $this->currentUrl)[0];
		$array = explode('/', $this->currentUrl);
		if(isset($array[2])) $this->currentUrl = $array[2]; else $this->currentUrl = 'specialists';
		
		if (isset($_GET['id']) && ($specId = intval($_GET['id'])) > 0) {
			$s = DB::select("SELECT id FROM specialists");
			$exists = 0;
			foreach ($s as $se) {
				if ($se->id == $specId) {
					$exists = 1;
				}
			}
			if ($exists == 0) {
				return redirect("/{$this->language}/specialists");
			}
			$specialist = DB::select("SELECT name, surname, prof_{$this->language} as prof, path FROM specialists WHERE id = {$specId}")[0];
			$spec_section = DB::select("SELECT section.id as sec_id, section.name_{$this->language} as name, section.subsection as subs, spec_info.description_{$this->language} as info 
				FROM section INNER JOIN spec_info ON section.id = spec_info.section_id 
				WHERE section.section = 'user' AND spec_info.s_id = {$specId}");
			$spec_s = DB::select("SELECT subserv_id FROM servis_spec WHERE spec_id = {$specId}");
			$spec_serv = [];
			foreach ($spec_s as $sepc_id){
				$spec_serv[] = intval($sepc_id->subserv_id);
			}
			$hav_sub_s = 0;
			foreach ($spec_serv as $s){
				$hav_sub_s = 1;
			}
			if ($hav_sub_s == 1) {
				$serv = DB::select("SELECT subservice.id as subId, subservice.name_{$this->language} as subName, subservice.serv_id as servId, subservice.course as courses, 
					subservice.price as price, services.name_{$this->language} as servName, subservice.min_price as min_price  
					FROM subservice INNER JOIN services ON subservice.serv_id = services.id 
					WHERE subservice.id IN (".implode(",",$spec_serv).") AND subservice.course = 0 AND subservice.hide = 0 AND services.hide = 0 ORDER BY services.name_{$this->language} ASC, subservice.name_{$this->language} ASC");
			} else {
				$serv = DB::select("SELECT subservice.id as subId, subservice.name_{$this->language} as subName, subservice.serv_id as servId, subservice.course as courses, 
					subservice.price as price, services.name_{$this->language} as servName, subservice.min_price as min_price  
					FROM subservice INNER JOIN services ON subservice.serv_id = services.id 
					WHERE subservice.id = -1 AND subservice.hide = 0 AND services.hide = 0 ORDER BY services.name_{$this->language} ASC, subservice.course ASC, subservice.name_{$this->language} ASC");
			}
			foreach ($this->translVal as $tr) {
				if ($tr->infoKey == "from") $from = $tr->infoVal;
			}
			return view('specialistOpen', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'currentUrl' => $this->currentUrl,
				'transl' => $from,
				'spec' => $specialist,
				'spec_section' => $spec_section,
				'services' => $serv,
				'id' => $_GET['id'],
			]);
		} else {
			$specialists = DB::select("SELECT id, prof_{$this->language} as prof, path, name, surname 
				FROM specialists WHERE hide = 0 ORDER BY surname ASC, name ASC");
			return view('specialists', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'specialists' => $specialists,
				'currentUrl' => $this->currentUrl,
			]);
		}
	}
	
	public function shop(){
		HomeController::langURL();
		HomeController::transl();
		// Active menu element
		$this->currentUrl = $_SERVER['REQUEST_URI'];
		$this->currentUrl = explode('?', $this->currentUrl)[0];
		$array = explode('/', $this->currentUrl);
		if(isset($array[2])) $this->currentUrl = $array[2]; else $this->currentUrl = 'shop';
		
		if (isset($_GET['id']) && ($prodId = intval($_GET['id'])) > 0) {
			$product = DB::select("SELECT id, name_{$this->language} as name, description_{$this->language} as desctription, price, path FROM products WHERE id = {$prodId}")[0];
			//$services = DB::select("SELECT id, name_{$this->language} as title FROM services ORDER BY name_{$this->language} ASC");
			
			return view('productOpen', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'product' => $product,
				//'services' => $services,
				'currentUrl' => $this->currentUrl,
				'id' => $_GET['id'],
			]);
		} else {
			$products = DB::select("SELECT products.id as id, products.name_{$this->language} as name, products.price as price, 
					products.path as path, products.serv_id as serv_id, services.name_{$this->language} as serv_name 
					FROM products INNER JOIN services ON products.serv_id = services.id ORDER BY services.name_{$this->language} ASC, products.name_{$this->language} ASC");
			
			return view('products', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'products' => $products,
				'currentUrl' => $this->currentUrl,
				'id' => 0,
			]);
		}
		
	}
	
	public function courses(){
		HomeController::langURL();
		HomeController::transl();
		// Active menu element
		$this->currentUrl = $_SERVER['REQUEST_URI'];
		$this->currentUrl = explode('?', $this->currentUrl)[0];
		$array = explode('/', $this->currentUrl);
		if(isset($array[2])) $this->currentUrl = $array[2]; else $this->currentUrl = 'courses';
		
		if (isset($_GET['courseId']) && ($courseId = intval($_GET['courseId'])) > 0) {
			$s = DB::select("SELECT id, course, hide FROM subservice");
			$exists = 0;
			foreach ($s as $se) {
				if ($se->id == $courseId && $se->course == 1 && $se->hide == 0) {
					$exists = 1;
				}
			}
			if ($exists == 0) {
				return redirect("/{$this->language}/courses");
			}
			$price = "";
			foreach ($this->translVal as $tr) {
				if ($tr->infoKey == "price") $price = $tr->infoVal;
			}
			$from = "";
			foreach ($this->translVal as $tr) {
				if ($tr->infoKey == "from") $from = $tr->infoVal;
			}
			$dur = "";
			foreach ($this->translVal as $tr) {
				if ($tr->infoKey == "duration") $dur = $tr->infoVal;
			}
			$agreem = "";
			foreach ($this->translVal as $tr) {
				if ($tr->infoKey == "by_agreement") $agreem = $tr->infoVal;
			}
			$subservice = DB::select("SELECT *, name_{$this->language} as title, description_{$this->language} as info FROM subservice WHERE id = {$courseId} AND hide = 0")[0];
			$specialists = DB::select("SELECT servis_spec.subserv_id as subServId, 
						specialists.name as name, specialists.surname as surname, specialists.id as sepcId, 
						specialists.path as path, specialists.prof_{$this->language} as prof 
						FROM specialists INNER JOIN servis_spec ON servis_spec.spec_id = specialists.id 
						WHERE servis_spec.subserv_id = {$courseId} AND specialists.hide = 0 ORDER BY specialists.surname ASC, specialists.name ASC");
			$spec = "";
			$ar = array();
			foreach ($specialists as $i) {
				array_push($ar, "<a href=\"/{$this->language}/specialists/specialist?id={$i->sepcId}\">{$i->name} {$i->surname} (<i class=\"prof_s\">{$i->prof}</i>)</a>");
			}
			$spec = implode(", ", $ar);
			
			return view('courseOpen', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'transl' => $this->translVal,
				'price' => $price,
				'from' => $from,
				'dur' => $dur,
				'agreem' => $agreem,
				'spec' => $spec,
				'subserv' => $subservice,
				'currentUrl' => $this->currentUrl,
				'id' => $_GET['courseId'],
			]);
		} else {
			$courses = DB::select("SELECT id, name_{$this->language} as title, min_price, price, duration FROM subservice WHERE course = 1 AND hide = 0");
			$price = "";
			foreach ($this->translVal as $tr) {
				if ($tr->infoKey == "price") $price = $tr->infoVal;
			}
			$from = "";
			foreach ($this->translVal as $tr) {
				if ($tr->infoKey == "from") $from = $tr->infoVal;
			}
			$dur = "";
			foreach ($this->translVal as $tr) {
				if ($tr->infoKey == "duration") $dur = $tr->infoVal;
			}
			$agreem = "";
			foreach ($this->translVal as $tr) {
				if ($tr->infoKey == "by_agreement") $agreem = $tr->infoVal;
			}
			$specialists = array();
			foreach ($courses as $c) {
				$specialists[$c->id] = DB::select("SELECT specialists.name as name, specialists.surname as surname, specialists.id as sepcId, 
						specialists.prof_{$this->language} as prof 
						FROM specialists INNER JOIN servis_spec ON servis_spec.spec_id = specialists.id 
						WHERE servis_spec.subserv_id = {$c->id} AND specialists.hide = 0 ORDER BY specialists.surname ASC, specialists.name ASC");
			}
			$spec = array();
			foreach ($specialists as $iKey => $iVal) {
				$spec[$iKey] = "";
				$ar = array();
				foreach ($iVal as $i) {
					array_push($ar, "<a href=\"/{$this->language}/specialists/specialist?id={$i->sepcId}\">{$i->name} {$i->surname} (<i class=\"prof_s\">{$i->prof}</i>)</a>");
				}
				$spec[$iKey] = implode(", ", $ar);
			}
			return view('courses', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'courses' => $courses,
				'price' => $price,
				'from' => $from,
				'dur' => $dur,
				'agreem' => $agreem,
				'spec' => $spec,
				'currentUrl' => $this->currentUrl,
			]);
		}
		
	}
	
	public function bachFlowers() {
		HomeController::langURL();
		HomeController::transl();
		// Active menu element
		$this->currentUrl = $_SERVER['REQUEST_URI'];
		$this->currentUrl = explode('?', $this->currentUrl)[0];
		$array = explode('/', $this->currentUrl);
		if(isset($array[2])) $this->currentUrl = $array[2]; else $this->currentUrl = 'bach-flowers';
		$bach = DB::select("SELECT content_{$this->language} as content FROM pages WHERE name = '{$this->currentUrl}'")[0];
		
		return view('bach', [
			'menu' => $this->menu,
			'lvUrl' => $this->lvUrl,
			'enUrl' => $this->enUrl,
			'ruUrl' => $this->ruUrl,
			'lang' => $this->language,
			'footer' => $this->footerTransl,
			'bach' => $bach,
			'currentUrl' => $this->currentUrl,
		]);
	}
	
	public function contacts() {
		HomeController::langURL();
		HomeController::transl();
		// Active menu element
		$this->currentUrl = $_SERVER['REQUEST_URI'];
		$this->currentUrl = explode('?', $this->currentUrl)[0];	
		$array = explode('/', $this->currentUrl);
		if(isset($array[2])) $this->currentUrl = $array[2]; else $this->currentUrl = 'contacts';
		$transp = DB::select("SELECT * FROM transports ORDER BY type");
		$bus = 0;
		$trol = 0;
		$tram = 0;
		foreach ($transp as $tr) {
			if ($tr->type == 1) $bus = $bus + 1;
			else if ($tr->type == 2) $trol = $trol + 1;
			else if ($tr->type == 2) $tram = $tram + 1;
		}
		
		return view('contacts', [
			'menu' => $this->menu,
			'lvUrl' => $this->lvUrl,
			'enUrl' => $this->enUrl,
			'ruUrl' => $this->ruUrl,
			'lang' => $this->language,
			'footer' => $this->footerTransl,
			'transl' => $this->translVal,
			'transp' => $transp,
			'bus' => $bus,
			'trol' => $trol,
			'tram' => $tram,
			'currentUrl' => $this->currentUrl,
		]);
	}
	
	
	public function langURL() {
		// LV url
		$this->lvUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $this->lvUrl);
		if($array[1] != 'lv') $array[1] = 'lv';
		$this->lvUrl = implode('/', $array);
		// EN url
		$this->enUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $this->enUrl);
		if($array[1] != 'en') $array[1] = 'en';
		$this->enUrl = implode('/', $array);
		// RU url
		$this->ruUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $this->ruUrl);
		if($array[1] != 'ru') $array[1] = 'ru';
		$this->ruUrl = implode('/', $array);		
	}
	public function transl() {
		$this->footerTransl = DB::select("SELECT name as info, name_{$this->language} as infoKey FROM transl");
		$this->translVal = DB::select("SELECT name as infoKey, name_{$this->language} as infoVal FROM transl");
		foreach ($this->footerTransl as $elemKey => $elem) {
			if (Schema::hasColumn("contacts", "{$elem->info}")) {
				$elem->info = DB::select("SELECT {$elem->info} as infoVal FROM contacts");
			} else {
				unset($this->footerTransl[$elemKey]);
			}
		}
	}
}
