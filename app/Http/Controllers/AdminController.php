<?php

namespace App\Http\Controllers;

use Auth;
use resources\views\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use DB;
use URL;
use Carbon\Carbon;
use Image;
use Imagine;
use Config;
use Schema;


class AdminController extends Controller
{
    private $language = 'lv';
	private $lvUrl = '';
	private $enUrl = '';
	private $ruUrl = '';
	private $currentUrl = '';
	
    public function __construct() {
        //$this->middleware('auth');
		$this->currentUrl = $_SERVER['REQUEST_URI'];
        $this->currentUrl = explode('?', $this->currentUrl)[0];
		
		// Get Language
		$this->language = explode('/', $this->currentUrl)[1];
		if ($this->language != 'en' && $this->language != 'ru') {
			$this->language = 'lv';
		} 
		// Get main menu
        $this->menu = DB::select("SELECT *, name_{$this->language} as title FROM menu ORDER BY ord");
		AdminController::transl();
    }
	
	public function index() {
		if (!Auth::check()) { return redirect('login'); }
		AdminController::langURL();
		AdminController::transl();
		// Active menu element
		$currentUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $currentUrl);
		if(isset($array[2])) $currentUrl = $array[2]; else $currentUrl = 'home';
		$home = DB::select("SELECT * FROM pages WHERE name = '{$currentUrl}'")[0];
		
		return view('admin.home', [
			'menu' => $this->menu,
			'lvUrl' => $this->lvUrl,
			'enUrl' => $this->enUrl,
			'ruUrl' => $this->ruUrl,
			'lang' => $this->language,
			'footer' => $this->footerTransl,
			'home' => $home,
			'currentUrl' => $currentUrl,
		]);
	}
	public function homeSave(Request $request) {
		if (!Auth::check()) { return redirect('login'); }
		if (isset($_GET['pageId']) && ($pageId = intval($_GET['pageId'])) > 0) {
			$affelcted = DB::update("UPDATE pages SET content_lv = '{$request->input('content_lv')}' WHERE id = {$pageId}")[0];
			$affelcted = DB::update("UPDATE pages SET content_en = '{$request->input('content_en')}' WHERE id = {$pageId}")[0];
			$affelcted = DB::update("UPDATE pages SET content_ru = '{$request->input('content_ru')}' WHERE id = {$pageId}")[0];
			$page = DB::select("SELECT name FROM pages WHERE id = {$pageId}")[0];
			
			$url = "/admin/{$page->name}";
			return redirect($url);
		}
		$url = "/admin/home";
		return redirect($url);
	}
	
	public function about() {
		if (!Auth::check()) { return redirect('login'); }
		AdminController::langURL();
		AdminController::transl();
		// Active menu element
		$currentUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $currentUrl);
		if(isset($array[2])) $currentUrl = $array[2]; else $currentUrl = 'about-us';
		$about = DB::select("SELECT * FROM pages WHERE name = '{$currentUrl}'")[0];
		$spec_section = DB::select("SELECT section.id as sec_id, section.name_lv as name_lv, section.name_en as name_en, section.name_ru as name_ru, 
			section.subsection as subs, spec_info.* 
			FROM section INNER JOIN spec_info ON section.id = spec_info.section_id 
			WHERE section.section = 'about'");
		
		return view('admin.about', [
			'menu' => $this->menu,
			'lvUrl' => $this->lvUrl,
			'enUrl' => $this->enUrl,
			'ruUrl' => $this->ruUrl,
			'lang' => $this->language,
			'footer' => $this->footerTransl,
			'about' => $about,
			'spec_section' => $spec_section,
			'currentUrl' => $currentUrl,
		]);
	}
	public function aboutSave(Request $request) {
		if (!Auth::check()) { return redirect('login'); }
		if (isset($_GET['pageId']) && ($pageId = intval($_GET['pageId'])) > 0) {
			$affelcted = DB::update("UPDATE pages SET content_lv = '{$request->input('content_lv')}' WHERE id = {$pageId}")[0];
			$affelcted = DB::update("UPDATE pages SET content_en = '{$request->input('content_en')}' WHERE id = {$pageId}")[0];
			$affelcted = DB::update("UPDATE pages SET content_ru = '{$request->input('content_ru')}' WHERE id = {$pageId}")[0];
			$page = DB::select("SELECT name FROM pages WHERE id = {$pageId}")[0];
			$info = DB::select("SELECT spec_info.id as infoId, section.subsection as subs 
				FROM section INNER JOIN spec_info ON section.id = spec_info.section_id  WHERE section.section = 'about'");
			$is_set = array("cg" => 0, "cv" => 0, "be" => 0, "wd" => 0);
			foreach ($info as $i) {
				if ($i->subs == "collective_goal") {
					$is_set['cg'] = 1;
					$affelcted = DB::update("UPDATE spec_info SET description_lv = '{$request->input('collective_goal_lv')}' WHERE id = '{$i->infoId}'")[0];
					$affelcted = DB::update("UPDATE spec_info SET description_en = '{$request->input('collective_goal_en')}' WHERE id = '{$i->infoId}'")[0];
					$affelcted = DB::update("UPDATE spec_info SET description_ru = '{$request->input('collective_goal_ru')}' WHERE id = '{$i->infoId}'")[0];
				} else if ($i->subs == "collective_vision") {
					$is_set['cv'] = 1;
					$affelcted = DB::update("UPDATE spec_info SET description_lv = '{$request->input('collective_vision_lv')}' WHERE id = '{$i->infoId}'")[0];
					$affelcted = DB::update("UPDATE spec_info SET description_en = '{$request->input('collective_vision_en')}' WHERE id = '{$i->infoId}'")[0];
					$affelcted = DB::update("UPDATE spec_info SET description_ru = '{$request->input('collective_vision_ru')}' WHERE id = '{$i->infoId}'")[0];
				} else if ($i->subs == "believes") {
					$is_set['be'] = 1;
					$affelcted = DB::update("UPDATE spec_info SET description_lv = '{$request->input('believes_lv')}' WHERE id = '{$i->infoId}'")[0];
					$affelcted = DB::update("UPDATE spec_info SET description_en = '{$request->input('believes_en')}' WHERE id = '{$i->infoId}'")[0];
					$affelcted = DB::update("UPDATE spec_info SET description_ru = '{$request->input('believes_ru')}' WHERE id = '{$i->infoId}'")[0];
				} else if ($i->subs == "will_do") {
					$is_set['wd'] = 1;
					$affelcted = DB::update("UPDATE spec_info SET description_lv = '{$request->input('will_do_lv')}' WHERE id = '{$i->infoId}'")[0];
					$affelcted = DB::update("UPDATE spec_info SET description_en = '{$request->input('will_do_en')}' WHERE id = '{$i->infoId}'")[0];
					$affelcted = DB::update("UPDATE spec_info SET description_ru = '{$request->input('will_do_ru')}' WHERE id = '{$i->infoId}'")[0];
				}
			}
			$specId = -1;
			if ($is_set['cg'] == 0) {
				$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
					VALUES ({$specId}, 5, '{$request->input('collective_goal_lv')}', '{$request->input('collective_goal_en')}', '{$request->input('collective_goal_ru')}')");
			}
			if ($is_set['cv'] == 0) {
				$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
					VALUES ({$specId}, 6, '{$request->input('collective_vision_lv')}', '{$request->input('collective_vision_en')}', '{$request->input('collective_vision_ru')}')");
			}
			if ($is_set['be'] == 0) {
				$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
					VALUES ({$specId}, 7, '{$request->input('believes_lv')}', '{$request->input('believes_en')}', '{$request->input('believes_ru')}')");
			}
			if ($is_set['wd'] == 0) {
				$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
					VALUES ({$specId}, 8, '{$request->input('will_do_lv')}', '{$request->input('will_do_en')}', '{$request->input('will_do_ru')}')");
			}
			$url = "/admin/{$page->name}";
			return redirect($url);
		}
		$url = "/admin/about-us";
		return redirect($url);
	}
	
	public function services() {
		if (!Auth::check()) { return redirect('login'); }
		AdminController::langURL();
		AdminController::transl();
		// Active menu element
		$currentUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $currentUrl);
		if(isset($array[2])) $currentUrl = $array[2]; else $currentUrl = 'services';
		$services = DB::select("SELECT *, name_{$this->language} as title FROM services ORDER BY name_{$this->language} ASC");
		$subservices = DB::select("SELECT *, name_{$this->language} as title FROM subservice WHERE course = 0 ORDER BY name_{$this->language}");
		
		return view('admin.servicesEdit', [
			'menu' => $this->menu,
			'lvUrl' => $this->lvUrl,
			'enUrl' => $this->enUrl,
			'ruUrl' => $this->ruUrl,
			'lang' => $this->language,
			'footer' => $this->footerTransl,
			'serv' => $services,
			'subServ' => $subservices,
			'currentUrl' => $currentUrl,
		]);
	}
	public function servicesSave(Request $request){
		if (!Auth::check()) { return redirect('login'); }
		$serv = DB::select("SELECT *, name_{$this->language} as title FROM services ORDER BY name_{$this->language} ASC");
		$subServ = DB::select("SELECT *, name_{$this->language} as title FROM subservice ORDER BY name_{$this->language} ASC");
			
		if (!empty($_POST['hide_serv'])) {
			$updated_values = [];
			foreach ($_POST['hide_serv'] as $checked) {
				$updated_values[] = intval($checked);
			}
			DB::update("UPDATE services SET hide = 1");
			foreach ($updated_values as $val) {
				DB::update("UPDATE services SET hide = 0 WHERE id = {$val}");
			}
		}
		if (!empty($_POST['hide_sub_serv'])) {
			$updated_values = [];
			foreach ($_POST['hide_sub_serv'] as $checked) {
				$updated_values[] = intval($checked);
			}
			DB::update("UPDATE subservice SET hide = 1");
			DB::update("UPDATE subservice SET hide = 0 WHERE id IN (".implode(",",$updated_values).")");
		}
		$url = "/admin/services";
		return redirect($url);
	}
	public function serviceEdit() {
		if (!Auth::check()) { return redirect('login'); }
		AdminController::langURL();
		AdminController::transl();
		// Active menu element
		$currentUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $currentUrl);
		if(isset($array[2])) $currentUrl = $array[2]; else $currentUrl = 'services';
		
		if (isset($_GET['servId']) && ($servId = intval($_GET['servId'])) > 0) {
			$services = DB::select("SELECT * FROM services WHERE id = {$servId}")[0];
			$subservices = DB::select("SELECT id, hide, name_{$this->language} as title FROM subservice WHERE serv_id = {$servId}");
			
			return view('admin.serviceEdit', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'serv' => $services,
				'subServ' => $subservices,
				'currentUrl' => $currentUrl,
			]);
		} else {
			$services = DB::select("SELECT * FROM services")[0];
			$subservices = DB::select("SELECT id, hide, name_{$this->language} as title FROM subservice WHERE serv_id = 0");
			$services->id = 0;
			$services->name = "";
			$services->name_lv = "";
			$services->name_en = "";
			$services->name_ru = "";
			$services->hide = 0;
			
			return view('admin.serviceEdit', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'serv' => $services,
				'subServ' => $subservices,
				'currentUrl' => $currentUrl,
			]);
		}
		$url = "/admin/services";
		return redirect($url);
	}
	public function serviceSave(Request $request){
		if (!Auth::check()) { return redirect('login'); }
		if (isset($_GET['servId']) && ($servId = intval($_GET['servId'])) > 0) {
			$hide = 1;
			if (isset($_POST['hide_serv'])) $hide = 0;
			DB::update("UPDATE services SET hide = {$hide} WHERE id = {$servId}");
			
			
			DB::update("UPDATE subservice SET hide = 1 WHERE serv_id = {$servId}");
			if (!empty($_POST['hide_sub_serv'])) {
				$updated_values = [];
				foreach ($_POST['hide_sub_serv'] as $checked) {
					$updated_values[] = intval($checked);
				}
				DB::update("UPDATE subservice SET hide = 0 WHERE id IN (".implode(",",$updated_values).")");
			}
			$affected = DB::table("services")
				->where('id', $servId)
				->update([
					'name_lv' => $request->input('name_lv'),
					'name_en' => $request->input('name_en'),
					'name_ru' => $request->input('name_ru'),
				]);
			$affelcted = DB::update("UPDATE services SET description_lv = '{$request->input('description_lv')}' WHERE id = '{$servId}'")[0];
			$affelcted = DB::update("UPDATE services SET description_en = '{$request->input('description_en')}' WHERE id = '{$servId}'")[0];
			$affelcted = DB::update("UPDATE services SET description_ru = '{$request->input('description_ru')}' WHERE id = '{$servId}'")[0];
			
			$url = "/admin/services/service/edit?servId={$servId}";
			return redirect($url);
		} else if (isset($_GET['servId']) && ($servId = intval($_GET['servId'])) == 0) {
			$hide = 0;
			if (isset($_POST['hide_sub_serv'])) $hide = 1;
			$affected = DB::insert("INSERT INTO services (name_lv, name_en, name_ru, description_lv, description_en, description_ru, hide) 
				VALUES ('{$request->input('name_lv')}', '{$request->input('name_en')}', '{$request->input('name_ru')}', 
					'{$request->input('description_lv')}', '{$request->input('description_en')}', '{$request->input('description_ru')}', {$hide})");;
		}
		$url = "/admin/services";
		return redirect($url);
	}
	public function serviceDelete(){
		if (!Auth::check()) { return redirect('login'); }
		if (isset($_GET['servId']) && ($servId = intval($_GET['servId'])) > 0) {
			$subserv = DB::select("SELECT id FROM subservice WHERE serv_id = {$servId}");
			foreach ($subserv as $S) {
				$affected = DB::delete("DELETE FROM servis_spec WHERE subserv_id = {$S->d}");
			}
			$affected = DB::delete("DELETE FROM subservice WHERE serv_id = {$servId}");
			$affected = DB::delete("DELETE FROM services WHERE id = {$servId}")[0];
		}
		$url = "/admin/services";
		return redirect($url);
	}
	public function subServiceEdit() {
		if (!Auth::check()) { return redirect('login'); }
		AdminController::langURL();
		AdminController::transl();
		// Active menu element
		$currentUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $currentUrl);
		if(isset($array[2])) $currentUrl = $array[2]; else $currentUrl = 'services';
		
		if (isset($_GET['subServId']) && ($subServId = intval($_GET['subServId'])) > 0) {
			$subservices = DB::select("SELECT * FROM subservice WHERE id = {$subServId}")[0];
			$services = DB::select("SELECT id, name_{$this->language} as title FROM services ORDER BY name_{$this->language} ASC");
			$serv_spec = DB::select("SELECT * FROM servis_spec WHERE subserv_id = {$subServId}");
			$spec = DB::select("SELECT name, surname, id, prof_lv as prof FROM specialists");
			
			return view('admin.subServiceEdit', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'serv' => $services,
				'subServ' => $subservices,
				'serv_spec' => $serv_spec,
				'spec' => $spec,
				'currentUrl' => $currentUrl,
			]);
		} else if (isset($_GET['subServId']) && ($subServId = intval($_GET['subServId'])) == 0) {
			$subservices = DB::select("SELECT * FROM subservice")[0];
			$services = DB::select("SELECT id, name_{$this->language} as title FROM services ORDER BY name_{$this->language} ASC");
			$subservices->id = 0;
			$subservices->name_lv = "";
			$subservices->name_en = "";
			$subservices->name_ru = "";
			$subservices->serv_id = -1;
			$subservices->hide = 0;
			$subservices->description_lv = "";
			$subservices->description_en = "";
			$subservices->description_ru = "";
			$serv_spec = DB::select("SELECT * FROM servis_spec");
			$spec = DB::select("SELECT name, surname, id, prof_lv as prof FROM specialists");
			
			return view('admin.subServiceEdit', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'serv' => $services,
				'subServ' => $subservices,
				'serv_spec' => $serv_spec,
				'spec' => $spec,
				'currentUrl' => $currentUrl,
			]);
		}
		$url = "/admin/services";
		return redirect($url);
	}
	public function subServiceSave(Request $request) {
		if (!Auth::check()) { return redirect('login'); }
		if (isset($_GET['subServId']) && ($subServId = intval($_GET['subServId'])) > 0) {
			$hide = 1;
			if (isset($_POST['hide_sub_serv'])) $hide = 0;
			$min_price = 0;
			if (isset($_POST['min_price'])) $min_price = 1;
			$affected = DB::table("subservice")
				->where('id', $subServId)
				->update([
					'name_lv' => $request->input('name_lv'),
					'name_en' => $request->input('name_en'),
					'name_ru' => $request->input('name_ru'),
					'serv_id' => $request->input('serv_id'),
					'price' => $request->input('price'),
					'duration' => $request->input('duration'),
					'hide' => $hide,
					'min_price' => $min_price,
				]);
			$affelcted = DB::update("UPDATE subservice SET description_lv = '{$request->input('description_lv')}' WHERE id = '{$subServId}'")[0];
			$affelcted = DB::update("UPDATE subservice SET description_en = '{$request->input('description_en')}' WHERE id = '{$subServId}'")[0];
			$affelcted = DB::update("UPDATE subservice SET description_ru = '{$request->input('description_ru')}' WHERE id = '{$subServId}'")[0];
			
			$serv_spec = [];
			foreach ($_POST['serv_spec'] as $sepc_id){
				$serv_spec[] = intval($sepc_id);
			}
			$specServ = DB::select("SELECT * FROM servis_spec WHERE subserv_id = {$subServId}");
			foreach ($specServ as $specS) {
				$giveServ = 0;
				foreach ($serv_spec as $key => $val) {
					if ($specS->spec_id == $val) {
						unset($serv_spec[$key]);
						$giveServ = 1;
					}
				}
				if ($giveServ == 0) {
					$affected = DB::delete("DELETE FROM servis_spec WHERE id = {$specS->id}")[0];
				}
			}
			foreach ($serv_spec as $key => $val) {
				$affected = DB::insert("INSERT INTO servis_spec (spec_id, subserv_id) VALUES ({$val}, {$subServId})");
			}
			
			$url = "/admin/services/subservice/edit?subServId={$subServId}";
			return redirect($url);
		} else if (isset($_GET['subServId']) && ($subServId = intval($_GET['subServId'])) == 0) {
			$hide = 1;
			if (isset($_POST['hide_sub_serv'])) $hide = 0;
			$min_price = 0;
			if (isset($_POST['min_price'])) $min_price = 1;
			$affected = DB::insert("INSERT INTO subservice (name_lv, name_en, name_ru, serv_id, description_lv, description_en, description_ru, hide, price, duration, min_price) 
				VALUES ('{$request->input('name_lv')}', '{$request->input('name_en')}', '{$request->input('name_ru')}', {$request->input('serv_id')},
				'{$request->input('description_lv')}', '{$request->input('description_en')}', '{$request->input('description_ru')}', {$hide}, 
				{$request->input('price')}, '{$request->input('duration')}', {$min_price})");
			
			$serv_spec = [];
			foreach ($_POST['serv_spec'] as $sepc_id){
				$serv_spec[] = intval($sepc_id);
			}
			$subs = DB::select("SELECT * FROM subservice WHERE name_lv = '{$request->input('name_lv')}' AND name_en = '{$request->input('name_en')}' AND name_ru = '{$request->input('name_ru')}'")[0];
			$subServId = $subs->id;
			$specServ = DB::select("SELECT * FROM servis_spec WHERE subserv_id = {$subServId}");
			foreach ($specServ as $specS) {
				$giveServ = 0;
				foreach ($serv_spec as $key => $val) {
					if ($specS->spec_id == $val) {
						unset($serv_spec[$key]);
						$giveServ = 1;
					}
				}
				if ($giveServ == 0) {
					$affected = DB::delete("DELETE FROM servis_spec WHERE id = {$specS->id}")[0];
				}
			}
			foreach ($serv_spec as $key => $val) {
				$affected = DB::insert("INSERT INTO servis_spec (spec_id, subserv_id) VALUES ({$val}, {$subServId})");
			}
			
			$url = "/admin/services";
			return redirect($url);
		}
		$url = "/admin/services";
		return redirect($url);
	}
	public function subServiceDelete() {
		if (!Auth::check()) { return redirect('login'); }
		if (isset($_GET['subServId']) && ($subServId = intval($_GET['subServId'])) > 0) {
			$serv = DB::select("SELECT serv_id FROM subservice WHERE id = {$subServId}")[0];
			$affected = DB::delete("DELETE FROM subservice WHERE id = {$subServId}");
			$affected = DB::delete("DELETE FROM servis_spec WHERE subserv_id = {$subServId}");
			$url = "/admin/services/service/edit?servId={$serv->serv_id}";
			return redirect($url);
		}
		$url = "/admin/services";
		return redirect($url);
	}
	
	public function pricelist() {
		if (!Auth::check()) { return redirect('login'); }
		AdminController::langURL();
		AdminController::transl();
		// Active menu element
		$currentUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $currentUrl);
		if(isset($array[2])) $currentUrl = $array[2]; else $currentUrl = 'priceplist';
		$serv = DB::select("SELECT subservice.id as subId, subservice.name_{$this->language} as subName, subservice.serv_id as servId, 
			subservice.price as price, services.name_{$this->language} as servName, subservice.duration as duration, subservice.min_price as min_price 
			FROM subservice INNER JOIN services ON subservice.serv_id = services.id 
			WHERE subservice.hide = 0 AND services.hide = 0 AND subservice.course = 0 ORDER BY services.name_{$this->language} ASC, subservice.name_{$this->language} ASC");
		
		$courses = DB::select("SELECT subservice.id as subId, subservice.name_{$this->language} as subName, subservice.serv_id as servId, 
			subservice.price as price, subservice.duration as duration, subservice.min_price as min_price 
			FROM subservice 
			WHERE subservice.hide = 0 AND subservice.course = 1 ORDER BY subservice.name_{$this->language} ASC");
		
		return view('admin.priceplist', [
			'menu' => $this->menu,
			'lvUrl' => $this->lvUrl,
			'enUrl' => $this->enUrl,
			'ruUrl' => $this->ruUrl,
			'lang' => $this->language,
			'footer' => $this->footerTransl,
			'services' => $serv,
			'courses' => $courses,
			'currentUrl' => $currentUrl,
		]);
	}
	public function pricelistSave(Request $request) {
		if (!Auth::check()) { return redirect('login'); }
		foreach ($_POST as $param_name => $param_val) {
			if ($param_name != "_token") {
				if (substr($param_name, 0, 4)=="dur_") {
					$id = explode('_', $param_name)[1];
					$affelcted = DB::update("UPDATE subservice SET duration = '{$param_val}' WHERE id = {$id}")[0];
				} else {
					$affelcted = DB::update("UPDATE subservice SET price = {$param_val} WHERE id = {$param_name}")[0];
				}
			}
		}
		$url = "/admin/pricelist";
		return redirect($url);
	}
	
	public function specialists() {
		if (!Auth::check()) { return redirect('login'); }
		AdminController::langURL();
		AdminController::transl();
		// Active menu element
		$currentUrl = $_SERVER['REQUEST_URI'];
		$currentUrl = explode('?', $currentUrl)[0];
		$array = explode('/', $currentUrl);
		if(isset($array[2])) $currentUrl = $array[2]; else $currentUrl = 'specialists';
		
		if (isset($_GET['id']) && ($specId = intval($_GET['id'])) > 0) {
			$specialist = DB::select("SELECT * FROM specialists WHERE id = {$specId}")[0];
			$spec_section = DB::select("SELECT section.id as sec_id, section.name_lv as name_lv, section.name_en as name_en, section.name_ru as name_ru, 
				section.subsection as subs, spec_info.* 
				FROM section INNER JOIN spec_info ON section.id = spec_info.section_id 
				WHERE section.section = 'user' AND spec_info.s_id = {$specId}");
			return view('admin.specialistOpen', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'spec' => $specialist,
				'spec_section' => $spec_section,
				'currentUrl' => $currentUrl,
				'id' => $_GET['id'],
			]);
		} else if (isset($_GET['id']) && ($specId = intval($_GET['id'])) == 0) {
			$specialist = DB::select("SELECT * FROM specialists")[0];
			$specialist->id = 0;
			$specialist->name = "";
			$specialist->surname = "";
			$specialist->description_lv = "";
			$specialist->description_en = "";
			$specialist->description_ru = "";
			$specialist->path = "";
			$specialist->prof_lv = "";
			$specialist->prof_en = "";
			$specialist->prof_ru = "";
			$specialist->hide = 0;
			$spec_section = DB::select("SELECT section.id as sec_id, section.name_lv as name_lv, section.name_en as name_en, section.name_ru as name_ru, 
				section.subsection as subs, spec_info.* 
				FROM section INNER JOIN spec_info ON section.id = spec_info.section_id 
				WHERE section.section = 'user' AND spec_info.s_id = {$specId}");
			return view('admin.specialistOpen', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'spec' => $specialist,
				'spec_section' => $spec_section,
				'currentUrl' => $currentUrl,
				'id' => 0,
			]);
		} else {
			$specialists = DB::select("SELECT * FROM specialists ORDER BY surname ASC, name ASC");
			return view('admin.specialists', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'specialists' => $specialists,
				'currentUrl' => $currentUrl,
			]);
		}
	}
	public function specialistSave(Request $request) {
		if (!Auth::check()) { return redirect('login'); }
		$path = "";
		$errors = [];
		$extension=array("jpeg","jpg","png");
		$target_dir = "images/profile/";
		$hide = 0;
		if (isset($_POST['hide'])) $hide = 1;
		if (isset($_FILES["picture"]["name"])) {
			$target_file = $target_dir . basename($_FILES["picture"]["name"]);
			$file_name=$_FILES["picture"]["name"];
			$file_tmp=$_FILES["picture"]["tmp_name"];
			$ext=pathinfo($file_name,PATHINFO_EXTENSION);
			if(in_array($ext,$extension))
			{
				if(!file_exists($target_file))
				{
					Image::make($file_tmp=$_FILES["picture"]["tmp_name"],array(
						'width' => 480,
						'height' => 640,
						'crop' => true
					))->save($target_file);
				} else {
					$filename=basename($file_name,$ext);
					$newFileName=$filename.time().".".$ext;
					$target_file = $target_dir . $newFileName;
					Image::make($file_tmp=$_FILES["picture"]["tmp_name"],array(
						'width' => 480,
						'height' => 640,
						'crop' => true
					))->save($target_file);
				}
				if (isset($_GET['id']) && ($specId = intval($_GET['id'])) > 0) {
					$affected = DB::table("specialists")
						->where('id', $specId)
						->update([
							'name' => $request->input('name'),
							'surname' => $request->input('surname'),
							'path' => $target_file,
							'prof_lv' => $request->input('prof_lv'),
							'prof_en' => $request->input('prof_en'),
							'prof_ru' => $request->input('prof_ru'),
							'hide' => $hide,
						]);
					$info = DB::select("SELECT spec_info.id as infoId, section.subsection as subs 
						FROM section INNER JOIN spec_info ON section.id = spec_info.section_id  WHERE section.section = 'user' AND spec_info.s_id = {$specId}");
					$is_set = array("wh" => 0, "ab" => 0, "ed" => 0, "wo" => 0);
					foreach ($info as $i) {
						if ($i->subs == "what_is") {
							$is_set['wh'] = 1;
							if ($request->input('what_is_lv') == "" && $request->input('what_is_en') == "" && $request->input('what_is_ru') == "") {
								$affected = DB::delete("DELETE FROM spec_info WHERE id = {$i->infoId}");
							} else {
								$affelcted = DB::update("UPDATE spec_info SET description_lv = '{$request->input('what_is_lv')}' WHERE id = '{$i->infoId}'")[0];
								$affelcted = DB::update("UPDATE spec_info SET description_en = '{$request->input('what_is_en')}' WHERE id = '{$i->infoId}'")[0];
								$affelcted = DB::update("UPDATE spec_info SET description_ru = '{$request->input('what_is_ru')}' WHERE id = '{$i->infoId}'")[0];
							}
						} else if ($i->subs == "about_me") {
							$is_set['ab'] = 1;
							if ($request->input('about_me_lv') == "" && $request->input('about_me_en') == "" && $request->input('about_me_ru') == "") {
								$affected = DB::delete("DELETE FROM spec_info WHERE id = {$i->infoId}");
							} else {
								$affelcted = DB::update("UPDATE spec_info SET description_lv = '{$request->input('about_me_lv')}' WHERE id = '{$i->infoId}'")[0];
								$affelcted = DB::update("UPDATE spec_info SET description_en = '{$request->input('about_me_en')}' WHERE id = '{$i->infoId}'")[0];
								$affelcted = DB::update("UPDATE spec_info SET description_ru = '{$request->input('about_me_ru')}' WHERE id = '{$i->infoId}'")[0];
							}
						} else if ($i->subs == "education") {
							$is_set['ed'] = 1;
							if ($request->input('education_lv') == "" && $request->input('education_en') == "" && $request->input('education_ru') == "") {
								$affected = DB::delete("DELETE FROM spec_info WHERE id = {$i->infoId}");
							} else {
								$affelcted = DB::update("UPDATE spec_info SET description_lv = '{$request->input('education_lv')}' WHERE id = '{$i->infoId}'")[0];
								$affelcted = DB::update("UPDATE spec_info SET description_en = '{$request->input('education_en')}' WHERE id = '{$i->infoId}'")[0];
								$affelcted = DB::update("UPDATE spec_info SET description_ru = '{$request->input('education_ru')}' WHERE id = '{$i->infoId}'")[0];
							}
						} else if ($i->subs == "work_history") {
							$is_set['wo'] = 1;
							if ($request->input('work_history_lv') == "" && $request->input('work_history_en') == "" && $request->input('work_history_ru') == "") {
								$affected = DB::delete("DELETE FROM spec_info WHERE id = {$i->infoId}");
							} else {
								$affelcted = DB::update("UPDATE spec_info SET description_lv = '{$request->input('work_history_lv')}' WHERE id = '{$i->infoId}'")[0];
								$affelcted = DB::update("UPDATE spec_info SET description_en = '{$request->input('work_history_en')}' WHERE id = '{$i->infoId}'")[0];
								$affelcted = DB::update("UPDATE spec_info SET description_ru = '{$request->input('work_history_ru')}' WHERE id = '{$i->infoId}'")[0];
							}
						}
					}
					if ($is_set['wh'] == 0) {
						if ($request->input('what_is_lv')!="" || $request->input('what_is_en')!= "" || $request->input('what_is_ru') != "") {
							$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
								VALUES ({$specId}, 1, '{$request->input('what_is_lv')}', '{$request->input('what_is_en')}', '{$request->input('what_is_ru')}')");
						}
					}
					if ($is_set['ab'] == 0) {
						if ($request->input('about_me_lv')!="" || $request->input('about_me_en')!= "" || $request->input('about_me_ru') != "") {
							$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
								VALUES ({$specId}, 2, '{$request->input('about_me_lv')}', '{$request->input('about_me_en')}', '{$request->input('about_me_ru')}')");
						}
					}
					if ($is_set['ed'] == 0) {
						if ($request->input('education_lv')!="" || $request->input('education_en')!= "" || $request->input('education_ru') != "") {
							$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
							 VALUES ({$specId}, 3, '{$request->input('education_lv')}', '{$request->input('education_en')}', '{$request->input('education_ru')}')");
						}
					}
					if ($is_set['wo'] == 0) {
						if ($request->input('work_history_lv')!="" || $request->input('work_history_en')!= "" || $request->input('work_history_ru') != "") {
							$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
							 VALUES ({$specId}, 4, '{$request->input('work_history_lv')}', '{$request->input('work_history_en')}', '{$request->input('work_history_ru')}')");
						}
					}
					$url = "/admin/specialists/specialist/edit?id={$specId}";
					return redirect($url);
				} else {
					$affected = DB::insert("INSERT INTO specialists (name, surname, path, prof_lv, prof_en, prof_ru, hide) 
						VALUES ('{$request->input('name')}', '{$request->input('surname')}', '{$target_file}', 
							'{$request->input('prof_lv')}', '{$request->input('prof_en')}', '{$request->input('prof_ru')}', {$hide})");
					$spec = DB::select("SELECT id FROM specialists WHERE name = '{$request->input('name')}' AND surname = '{$request->input('surname')}' AND path = '{$target_file}'")[0];
					if ($request->input('what_is_lv')!="" || $request->input('what_is_en')!= "" || $request->input('what_is_ru') != "") {
						$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
							VALUES ({$spec->id}, 1, '{$request->input('what_is_lv')}', '{$request->input('what_is_en')}', '{$request->input('what_is_ru')}')");
					}
					if ($request->input('about_me_lv')!="" || $request->input('about_me_en')!= "" || $request->input('about_me_ru') != "") {
						$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
							VALUES ({$spec->id}, 2, '{$request->input('about_me_lv')}', '{$request->input('about_me_en')}', '{$request->input('about_me_ru')}')");
					}
					if ($request->input('education_lv')!="" || $request->input('education_en')!= "" || $request->input('education_ru') != "") {
						$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
							VALUES ({$spec->id}, 3, '{$request->input('education_lv')}', '{$request->input('education_en')}', '{$request->input('education_ru')}')");
					}
					if ($request->input('work_history_lv')!="" || $request->input('work_history_en')!= "" || $request->input('work_history_ru') != "") {
						$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
							VALUES ({$spec->id}, 4, '{$request->input('work_history_lv')}', '{$request->input('work_history_en')}', '{$request->input('work_history_ru')}')");
					}
					$url = "/admin/specialists/specialist/edit?id={$spec->id}";
					return redirect($url);
				}
			}
		}
		if (isset($_GET['id']) && ($specId = intval($_GET['id'])) > 0) {
			$affected = $affected = DB::table("specialists")
				->where('id', $specId)
				->update([
					'name' => $request->input('name'),
					'surname' => $request->input('surname'),
					'prof_lv' => $request->input('prof_lv'),
					'prof_en' => $request->input('prof_en'),
					'prof_ru' => $request->input('prof_ru'),
					'hide' => $hide,
				]);
			$info = DB::select("SELECT spec_info.id as infoId, section.subsection as subs 
				FROM section INNER JOIN spec_info ON section.id = spec_info.section_id  WHERE section.section = 'user' AND spec_info.s_id = {$specId}");
			$is_set = array("wh" => 0, "ab" => 0, "ed" => 0, "wo" => 0);
			foreach ($info as $i) {
				if ($i->subs == "what_is") {
					$is_set['wh'] = 1;
					if ($request->input('what_is_lv') == "" && $request->input('what_is_en') == "" && $request->input('what_is_ru') == "") {
						$affected = DB::delete("DELETE FROM spec_info WHERE id = {$i->infoId}");
					} else {
						$affelcted = DB::update("UPDATE spec_info SET description_lv = '{$request->input('what_is_lv')}' WHERE id = '{$i->infoId}'")[0];
						$affelcted = DB::update("UPDATE spec_info SET description_en = '{$request->input('what_is_en')}' WHERE id = '{$i->infoId}'")[0];
						$affelcted = DB::update("UPDATE spec_info SET description_ru = '{$request->input('what_is_ru')}' WHERE id = '{$i->infoId}'")[0];
					}
				} else if ($i->subs == "about_me") {
					$is_set['ab'] = 1;
					if ($request->input('about_me_lv') == "" && $request->input('about_me_en') == "" && $request->input('about_me_ru') == "") {
						$affected = DB::delete("DELETE FROM spec_info WHERE id = {$i->infoId}");
					} else {
						$affelcted = DB::update("UPDATE spec_info SET description_lv = '{$request->input('about_me_lv')}' WHERE id = '{$i->infoId}'")[0];
						$affelcted = DB::update("UPDATE spec_info SET description_en = '{$request->input('about_me_en')}' WHERE id = '{$i->infoId}'")[0];
						$affelcted = DB::update("UPDATE spec_info SET description_ru = '{$request->input('about_me_ru')}' WHERE id = '{$i->infoId}'")[0];
					}
				} else if ($i->subs == "education") {
					$is_set['ed'] = 1;
					if ($request->input('education_lv') == "" && $request->input('education_en') == "" && $request->input('education_ru') == "") {
						$affected = DB::delete("DELETE FROM spec_info WHERE id = {$i->infoId}");
					} else {
						$affelcted = DB::update("UPDATE spec_info SET description_lv = '{$request->input('education_lv')}' WHERE id = '{$i->infoId}'")[0];
						$affelcted = DB::update("UPDATE spec_info SET description_en = '{$request->input('education_en')}' WHERE id = '{$i->infoId}'")[0];
						$affelcted = DB::update("UPDATE spec_info SET description_ru = '{$request->input('education_ru')}' WHERE id = '{$i->infoId}'")[0];
					}
				} else if ($i->subs == "work_history") {
					$is_set['wo'] = 1;
					if ($request->input('work_history_lv') == "" && $request->input('work_history_en') == "" && $request->input('work_history_ru') == "") {
						$affected = DB::delete("DELETE FROM spec_info WHERE id = {$i->infoId}");
					} else {
						$affelcted = DB::update("UPDATE spec_info SET description_lv = '{$request->input('work_history_lv')}' WHERE id = '{$i->infoId}'")[0];
						$affelcted = DB::update("UPDATE spec_info SET description_en = '{$request->input('work_history_en')}' WHERE id = '{$i->infoId}'")[0];
						$affelcted = DB::update("UPDATE spec_info SET description_ru = '{$request->input('work_history_ru')}' WHERE id = '{$i->infoId}'")[0];
					}
				}
			}
			if ($is_set['wh'] == 0) {
				if ($request->input('what_is_lv')!="" || $request->input('what_is_en')!= "" || $request->input('what_is_ru') != "") {
					$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
						VALUES ({$specId}, 1, '{$request->input('what_is_lv')}', '{$request->input('what_is_en')}', '{$request->input('what_is_ru')}')");
				}
			}
			if ($is_set['ab'] == 0) {
				if ($request->input('about_me_lv')!="" || $request->input('about_me_en')!= "" || $request->input('about_me_ru') != "") {
					$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
						VALUES ({$specId}, 2, '{$request->input('about_me_lv')}', '{$request->input('about_me_en')}', '{$request->input('about_me_ru')}')");
				}
			}
			if ($is_set['ed'] == 0) {
				if ($request->input('education_lv')!="" || $request->input('education_en')!= "" || $request->input('education_ru') != "") {
					$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
						VALUES ({$specId}, 3, '{$request->input('education_lv')}', '{$request->input('education_en')}', '{$request->input('education_ru')}')");
				}
			}
			if ($is_set['wo'] == 0) {
				if ($request->input('work_history_lv')!="" || $request->input('work_history_en')!= "" || $request->input('work_history_ru') != "") {
					$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
						VALUES ({$specId}, 4, '{$request->input('work_history_lv')}', '{$request->input('work_history_en')}', '{$request->input('work_history_ru')}')");
				}
			}
			$url = "/admin/specialists/specialist/edit?id={$specId}";
			return redirect($url);
		} else {
			$affected = DB::insert("INSERT INTO specialists (name, surname, prof_lv, prof_en, prof_ru, hide) 
				VALUES ('{$request->input('name')}', '{$request->input('surname')}', '{$request->input('prof_lv')}', 
				'{$request->input('prof_en')}', '{$request->input('prof_ru')}', {$hide})");
			$spec = DB::select("SELECT id FROM specialists WHERE name = '{$request->input('name')}' AND surname = '{$request->input('surname')}' AND path = '{$target_file}'")[0];
			if ($request->input('what_is_lv')!="" || $request->input('what_is_en')!= "" || $request->input('what_is_ru') != "") {
				$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
					VALUES ({$spec->id}, 1, '{$request->input('what_is_lv')}', '{$request->input('what_is_en')}', '{$request->input('what_is_ru')}')");
			}
			if ($request->input('about_me_lv')!="" || $request->input('about_me_en')!= "" || $request->input('about_me_ru') != "") {
				$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
					VALUES ({$spec->id}, 2, '{$request->input('about_me_lv')}', '{$request->input('about_me_en')}', '{$request->input('about_me_ru')}')");
			}
			if ($request->input('education_lv')!="" || $request->input('education_en')!= "" || $request->input('education_ru') != "") {
				$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
					VALUES ({$spec->id}, 3, '{$request->input('education_lv')}', '{$request->input('education_en')}', '{$request->input('education_ru')}')");
			}
			if ($request->input('work_history_lv')!="" || $request->input('work_history_en')!= "" || $request->input('work_history_ru') != "") {
				$affected = DB::insert("INSERT INTO spec_info (s_id, section_id, description_lv, description_en, description_ru) 
					VALUES ({$spec->id}, 4, '{$request->input('work_history_lv')}', '{$request->input('work_history_en')}', '{$request->input('work_history_ru')}')");
			}
			$url = "/admin/specialists/specialist/edit?id={$spec->id}";
			return redirect($url);
		}
	}
	public function specialistImgDelete() {
		if (!Auth::check()) { return redirect('login'); }
		if (isset($_GET['id']) && ($specId = intval($_GET['id'])) > 0) {
			$specialist = DB::select("SELECT * FROM specialists WHERE id = {$specId}")[0];
			if ( $specialist->path != "") {
				$path = $specialist->path;
				$path = str_replace('/images', 'images', $path);
				if(file_exists($path))
					unlink($path);
				$affected = DB::table("specialists")
					->where('id', $specId)
					->update(['path' => '']);
			}
			$url = "/admin/sepcialists/sepcialist/edit?id={$specId}";
			return redirect($url);
		}
		$url = "/admin/sepcialists";
		return redirect($url);
	}
	public function specialistServices () {
		if (!Auth::check()) { return redirect('login'); }
		AdminController::langURL();
		AdminController::transl();
		// Active menu element
		$currentUrl = $_SERVER['REQUEST_URI'];
		$currentUrl = explode('?', $currentUrl)[0];
		$array = explode('/', $currentUrl);
		if(isset($array[2])) $currentUrl = $array[2]; else $currentUrl = 'specialists';
		
		if (isset($_GET['specId']) && ($specId = intval($_GET['specId'])) > 0) {
			$specialist = DB::select("SELECT * FROM specialists WHERE id = {$specId}")[0];
			$serv = DB::select("SELECT *, name_{$this->language} as title FROM services ORDER BY name_{$this->language} ASC");
			$subServ = DB::select("SELECT *, name_{$this->language} as title FROM subservice ORDER BY name_{$this->language} ASC");
			$specServ = DB::select("SELECT * FROM servis_spec WHERE spec_id = {$specId}");
			
			return view('admin.specialistServices', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'spec' => $specialist,
				'serv' => $serv,
				'subServ' => $subServ,
				'specServ' => $specServ,
				'currentUrl' => $currentUrl,
				'id' => $_GET['specId'],
			]);
		} else {
			$url = "/admin/sepcialists";
			return redirect($url);
		}
	}
	public function specialistServicesSave (Request $request) {
		if (!Auth::check()) { return redirect('login'); }
		if (isset($_GET['specId']) && ($specId = intval($_GET['specId'])) > 0) {
			$specialist = DB::select("SELECT * FROM specialists WHERE id = {$specId}")[0];
			$serv = DB::select("SELECT *, name_{$this->language} as title FROM services ORDER BY name_{$this->language} ASC");
			$subServ = DB::select("SELECT *, name_{$this->language} as title FROM subservice ORDER BY name_{$this->language} ASC");
			$specServ = DB::select("SELECT * FROM servis_spec WHERE spec_id = {$specId}");
			
			if (!empty($_POST['spec_sub_serv'])) {
				$updated_values = [];
				foreach ($_POST['spec_sub_serv'] as $checked) {
					$updated_values[] = intval($checked);
				}
				foreach ($specServ as $specS) {
					$giveServ = 0;
					foreach ($updated_values as $key => $val) {
						if ($specS->subserv_id == $val) {
							unset($updated_values[$key]);
							$giveServ = 1;
						}
					}
					if ($giveServ == 0) {
						$affected = DB::delete("DELETE FROM servis_spec WHERE id = {$specS->id}")[0];
					}
				}
				foreach ($updated_values as $key => $val) {
					$affected = DB::insert("INSERT INTO servis_spec (spec_id, subserv_id) VALUES ({$specId}, {$val})");
				}
			}
			$url = "/admin/specialists/specialist/services/edit?specId={$specId}";
			return redirect($url);
		} else {
			$url = "/admin/specialists";
			return redirect($url);
		}
	}
	
	public function shop(){
		if (!Auth::check()) { return redirect('login'); }
		AdminController::langURL();
		AdminController::transl();
		// Active menu element
		$currentUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $currentUrl);
		if(isset($array[2])) $currentUrl = $array[2]; else $currentUrl = 'contacts';
		
		if (isset($_GET['id']) && ($prodId = intval($_GET['id'])) > 0) {
			$product = DB::select("SELECT * FROM products WHERE id = {$prodId}")[0];
			$services = DB::select("SELECT id, name_{$this->language} as title FROM services ORDER BY name_{$this->language} ASC");
			
			return view('admin.productOpen', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'product' => $product,
				'services' => $services,
				'currentUrl' => $currentUrl,
				'id' => $_GET['id'],
			]);
		} else if (isset($_GET['id']) && ($specId = intval($_GET['id'])) == 0) {
			$product = DB::select("SELECT * FROM products")[0];
			$services = DB::select("SELECT id, name_{$this->language} as title FROM services ORDER BY name_{$this->language} ASC");
			$product->id = 0;
			$product->name_lv = "";
			$product->name_en = "";
			$product->name_ru = "";
			$product->description_lv = "";
			$product->description_en = "";
			$product->description_ru = "";
			$product->path = "";
			$product->serv_id = 0;
			$product->hide = 0;
			return view('admin.productOpen', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'product' => $product,
				'services' => $services,
				'currentUrl' => $currentUrl,
				'id' => 0,
			]);
		} else {
			$products = DB::select("SELECT products.id as id, products.name_{$this->language} as name, products.price as price, 
					products.path as path, products.serv_id as serv_id, services.name_{$this->language} as serv_name 
					FROM products INNER JOIN services ON products.serv_id = services.id ORDER BY services.name_{$this->language} ASC, products.name_{$this->language} ASC");
			
			return view('admin.products', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'products' => $products,
				'currentUrl' => $currentUrl,
				'id' => 0,
			]);
		}
	}
	public function shopSave(Request $request){
		if (!Auth::check()) { return redirect('login'); }
		$path = "";
		$errors = [];
		$extension=array("jpeg","jpg","png");
		$target_dir = "images/products/";
		$hide = 0;
		if (isset($_POST['hide'])) $hide = 1;
		if (isset($_FILES["picture"]["name"])) {
			$target_file = $target_dir . basename($_FILES["picture"]["name"]);
			$file_name=$_FILES["picture"]["name"];
			$file_tmp=$_FILES["picture"]["tmp_name"];
			$ext=pathinfo($file_name,PATHINFO_EXTENSION);
			if(in_array($ext,$extension))
			{
				if(!file_exists($target_file))
				{
					Image::make($file_tmp=$_FILES["picture"]["tmp_name"],array(
						'width' => 800,
						'height' => 800,
						'crop' => true
					))->save($target_file);
				} else {
					$filename=basename($file_name,$ext);
					$newFileName=$filename.time().".".$ext;
					$target_file = $target_dir . $newFileName;
					Image::make($file_tmp=$_FILES["picture"]["tmp_name"],array(
						'width' => 800,
						'height' => 800,
						'crop' => true
					))->save($target_file);
				}
				if (isset($_GET['id']) && ($prodId = intval($_GET['id'])) > 0) {
					$affected = DB::table("products")
						->where('id', $prodId)
						->update([
							'price' => $request->input('price'),
							'serv_id' => $request->input('serv_id'),
							'path' => $target_file,
							'name_lv' => $request->input('name_lv'),
							'name_en' => $request->input('name_en'),
							'name_ru' => $request->input('name_ru'),
							'hide' => $hide,
						]);
					$affelcted = DB::update("UPDATE products SET description_lv = '{$request->input('description_lv')}' WHERE id = '{$prodId}'")[0];
					$affelcted = DB::update("UPDATE products SET description_en = '{$request->input('description_en')}' WHERE id = '{$prodId}'")[0];
					$affelcted = DB::update("UPDATE products SET description_ru = '{$request->input('description_ru')}' WHERE id = '{$prodId}'")[0];
					$url = "/admin/shop/product/edit?id={$prodId}";
					return redirect($url);
				} else {
					$affected = DB::insert("INSERT INTO products (price, serv_id, name_lv, name_en, name_ru, path, hide, description_lv, description_en, description_ru) 
						VALUES ({$request->input('price')}, '{$request->input('serv_id')}', '{$request->input('name_lv')}', '{$request->input('name_en')}', 
						'{$request->input('name_ru')}', '{$target_file}', {$hide}, '{$request->input('description_lv')}', '{$request->input('description_en')}', '{$request->input('description_ru')}')");
					$prod = DB::select("SELECT id FROM products WHERE price = {$request->input('price')}, serv_id = {$request->input('serv_id')}, 
						name_lv = {$request->input('name_lv')}, name_en = {$request->input('name_en')}, name_ru = {$request->input('name_ru')}, 
						path = {$request->input('path')}, hide = {$hide}")[0];
					$url = "/admin/shop/product/edit?id={$prod->id}";
					return redirect($url);
				}
			}
		}
		if (isset($_GET['id']) && ($prodId = intval($_GET['id'])) > 0) {
			$affected = DB::table("products")
				->where('id', $prodId)
				->update([
					'price' => $request->input('price'),
					'serv_id' => $request->input('serv_id'),
					'name_lv' => $request->input('name_lv'),
					'name_en' => $request->input('name_en'),
					'name_ru' => $request->input('name_ru'),
					'hide' => $hide,
				]);
			$affelcted = DB::update("UPDATE products SET description_lv = '{$request->input('description_lv')}' WHERE id = '{$prodId}'")[0];
			$affelcted = DB::update("UPDATE products SET description_en = '{$request->input('description_en')}' WHERE id = '{$prodId}'")[0];
			$affelcted = DB::update("UPDATE products SET description_ru = '{$request->input('description_ru')}' WHERE id = '{$prodId}'")[0];
			$url = "/admin/shop/product/edit?id={$prodId}";
			return redirect($url);
		} else if (isset($_GET['id']) && ($prodId = intval($_GET['id'])) == 0) {
			$affected = DB::insert("INSERT INTO products (price, serv_id, name_lv, name_en, name_ru, hide, description_lv, description_en, description_ru) 
				VALUES ({$request->input('price')}, '{$request->input('serv_id')}', '{$request->input('name_lv')}', '{$request->input('name_en')}', 
				'{$request->input('name_ru')}', {$hide}, '{$request->input('description_lv')}', '{$request->input('description_en')}', '{$request->input('description_ru')}')");
			$prod = DB::select("SELECT id FROM products WHERE price = {$request->input('price')}, serv_id = {$request->input('serv_id')}, 
				name_lv = {$request->input('name_lv')}, name_en = {$request->input('name_en')}, name_ru = {$request->input('name_ru')}, 
				hide = {$hide}")[0];
			$url = "/admin/shop/product/edit?id={$prod->id}";
			return redirect($url);
		} else {
			$url = "/admin/shop";
			return redirect($url);
		}
	}
	public function shopProdImgDelete() {
		if (!Auth::check()) { return redirect('login'); }
		if (isset($_GET['id']) && ($prodId = intval($_GET['id'])) > 0) {
			$s = DB::select("SELECT id FROM products");
			$exists = 0;
			foreach ($s as $se) {
				if ($se->id == $prodId) {
					$exists = 1;
				}
			}
			if ($exists == 0) {
				return redirect("/admin/shop");
			}
			$product = DB::select("SELECT * FROM products WHERE id = {$prodId}")[0];
			if ( $product->path != "") {
				$path = $product->path;
				$path = str_replace('/images', 'images', $path);
				if(file_exists($path))
					unlink($path);
				$affected = DB::table("products")
					->where('id', $prodId)
					->update(['path' => '']);
			}
			$url = "/admin/shop/product/edit?id={$prodId}";
			return redirect($url);
		}
		return redirect("/admin/shop");
	}
	
	public function courses(){
		if (!Auth::check()) { return redirect('login'); }
		AdminController::langURL();
		AdminController::transl();
		// Active menu element
		$currentUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $currentUrl);
		if(isset($array[2])) $currentUrl = $array[2]; else $currentUrl = 'courses';
		
		if (isset($_GET['courseId']) && ($courseId = intval($_GET['courseId'])) > 0) {
			$subservices = DB::select("SELECT * FROM subservice WHERE id = {$courseId}")[0];
			$services = DB::select("SELECT id, name_{$this->language} as title FROM services ORDER BY name_{$this->language} ASC");
			$serv_spec = DB::select("SELECT * FROM servis_spec WHERE subserv_id = {$courseId}");
			$spec = DB::select("SELECT name, surname, id, prof_lv as prof FROM specialists");
			
			return view('admin.courseOpen', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'serv' => $services,
				'subServ' => $subservices,
				'serv_spec' => $serv_spec,
				'spec' => $spec,
				'currentUrl' => $currentUrl,
				'id' => $_GET['courseId'],
			]);
		} else if (isset($_GET['courseId']) && ($specId = intval($_GET['courseId'])) == 0) {
			$subservices = DB::select("SELECT * FROM subservice")[0];
			$services = DB::select("SELECT id, name_{$this->language} as title FROM services ORDER BY name_{$this->language} ASC");
			$subservices->id = 0;
			$subservices->name = "";
			$subservices->name_lv = "";
			$subservices->name_en = "";
			$subservices->name_ru = "";
			$subservices->serv_id = -1;
			$subservices->hide = 0;
			$subservices->description_lv = "";
			$subservices->description_en = "";
			$subservices->description_ru = "";
			$serv_spec = DB::select("SELECT * FROM servis_spec");
			$spec = DB::select("SELECT name, surname, id, prof_lv as prof FROM specialists");
			
			return view('admin.courseOpen', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'serv' => $services,
				'subServ' => $subservices,
				'serv_spec' => $serv_spec,
				'spec' => $spec,
				'currentUrl' => $currentUrl,
				'id' => 0,
			]);
		} else {
			$courses = DB::select("SELECT name_{$this->language} as name, id, hide FROM subservice WHERE course = 1 ORDER BY name_{$this->language} ASC");
			
			return view('admin.courses', [
				'menu' => $this->menu,
				'lvUrl' => $this->lvUrl,
				'enUrl' => $this->enUrl,
				'ruUrl' => $this->ruUrl,
				'lang' => $this->language,
				'footer' => $this->footerTransl,
				'courses' => $courses,
				'currentUrl' => $currentUrl,
				'id' => 0,
			]);
		}
	}
	public function courseSave(Request $request) {
		if (!Auth::check()) { return redirect('login'); }
		if (isset($_GET['courseId']) && ($courseId = intval($_GET['courseId'])) > 0) {
			$hide = 1;
			if (isset($_POST['hide_sub_serv'])) $hide = 0;
			$min_price = 0;
			if (isset($_POST['min_price'])) $min_price = 1;
			$affected = DB::table("subservice")
				->where('id', $courseId)
				->update([
					'name_lv' => $request->input('name_lv'),
					'name_en' => $request->input('name_en'),
					'name_ru' => $request->input('name_ru'),
					'serv_id' => -1,
					'price' => $request->input('price'),
					'duration' => $request->input('duration'),
					'hide' => $hide,
					'min_price' => $min_price,
				]);
			$affelcted = DB::update("UPDATE subservice SET description_lv = '{$request->input('description_lv')}' WHERE id = '{$courseId}'")[0];
			$affelcted = DB::update("UPDATE subservice SET description_en = '{$request->input('description_en')}' WHERE id = '{$courseId}'")[0];
			$affelcted = DB::update("UPDATE subservice SET description_ru = '{$request->input('description_ru')}' WHERE id = '{$courseId}'")[0];
			
			$serv_spec = [];
			foreach ($_POST['serv_spec'] as $sepc_id){
				$serv_spec[] = intval($sepc_id);
			}
			$specServ = DB::select("SELECT * FROM servis_spec WHERE subserv_id = {$courseId}");
			foreach ($specServ as $specS) {
				$giveServ = 0;
				foreach ($serv_spec as $key => $val) {
					if ($specS->spec_id == $val) {
						unset($serv_spec[$key]);
						$giveServ = 1;
					}
				}
				if ($giveServ == 0) {
					$affected = DB::delete("DELETE FROM servis_spec WHERE id = {$specS->id}")[0];
				}
			}
			foreach ($serv_spec as $key => $val) {
				$affected = DB::insert("INSERT INTO servis_spec (spec_id, subserv_id) VALUES ({$val}, {$courseId})");
			}
			
			$url = "/admin/courses/course/edit?courseId={$courseId}";
			return redirect($url);
		} else if (isset($_GET['courseId']) && ($courseId = intval($_GET['courseId'])) == 0) {
			$hide = 1;
			if (isset($_POST['hide_sub_serv'])) $hide = 0;
			$min_price = 0;
			if (isset($_POST['min_price'])) $min_price = 1;
			$affected = DB::insert("INSERT INTO subservice (name_lv, name_en, name_ru, serv_id, description_lv, description_en, description_ru, hide, price, duration, min_price, course) 
				VALUES ('{$request->input('name_lv')}', '{$request->input('name_en')}', '{$request->input('name_ru')}', -1,
				'{$request->input('description_lv')}', '{$request->input('description_en')}', '{$request->input('description_ru')}', {$hide}, 
				{$request->input('price')}, '{$request->input('duration')}', {$min_price}), 1");
			
			$serv_spec = [];
			foreach ($_POST['serv_spec'] as $sepc_id){
				$serv_spec[] = intval($sepc_id);
			}
			$subs = DB::select("SELECT * FROM subservice WHERE name_lv = '{$request->input('name_lv')}' AND name_en = '{$request->input('name_en')}' AND name_ru = '{$request->input('name_ru')}'")[0];
			$courseId = $subs->id;
			$specServ = DB::select("SELECT * FROM servis_spec WHERE subserv_id = {$courseId}");
			foreach ($specServ as $specS) {
				$giveServ = 0;
				foreach ($serv_spec as $key => $val) {
					if ($specS->spec_id == $val) {
						unset($serv_spec[$key]);
						$giveServ = 1;
					}
				}
				if ($giveServ == 0) {
					$affected = DB::delete("DELETE FROM servis_spec WHERE id = {$specS->id}")[0];
				}
			}
			foreach ($serv_spec as $key => $val) {
				$affected = DB::insert("INSERT INTO servis_spec (spec_id, subserv_id) VALUES ({$val}, {$courseId})");
			}
			
			$url = "/admin/courses/course/edit?courseId={$courseId}";
			return redirect($url);
		}
		$url = "/admin/courses";
		return redirect($url);
	}
	public function coursesSave(Request $request){
		if (!Auth::check()) { return redirect('login'); }
		$serv = DB::select("SELECT *, name_{$this->language} as title FROM services ORDER BY name_{$this->language} ASC");
		$subServ = DB::select("SELECT *, name_{$this->language} as title FROM subservice ORDER BY name_{$this->language} ASC");
			
		if (!empty($_POST['hide_course'])) {
			$updated_values = [];
			foreach ($_POST['hide_course'] as $checked) {
				$updated_values[] = intval($checked);
			}
			DB::update("UPDATE subservice SET hide = 1 WHERE hide >=0 AND serv_id = -1 AND course = 1");
			foreach ($updated_values as $val) {
				DB::update("UPDATE subservice SET hide = 0 WHERE id = {$val}");
			}
		} else {
			DB::update("UPDATE subservice SET hide = 1 WHERE hide >=0 AND serv_id = -1 AND course = 1");
		}
		$url = "/admin/courses";
		return redirect($url);
	}
	
	public function bachFlowers() {
		if (!Auth::check()) { return redirect('login'); }
		AdminController::langURL();
		AdminController::transl();
		// Active menu element
		$currentUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $currentUrl);
		if(isset($array[2])) $currentUrl = $array[2]; else $currentUrl = 'bach-flowers';
		$bach = DB::select("SELECT * FROM pages WHERE name = '{$currentUrl}'")[0];
		
		return view('admin.bach', [
			'menu' => $this->menu,
			'lvUrl' => $this->lvUrl,
			'enUrl' => $this->enUrl,
			'ruUrl' => $this->ruUrl,
			'lang' => $this->language,
			'footer' => $this->footerTransl,
			'bach' => $bach,
			'currentUrl' => $currentUrl,
		]);
	}
	public function bachFlowersSave(Request $request) {
		if (!Auth::check()) { return redirect('login'); }
		if (isset($_GET['pageId']) && ($pageId = intval($_GET['pageId'])) > 0) {
			$affelcted = DB::update("UPDATE pages SET content_lv = '{$request->input('content_lv')}' WHERE id = {$pageId}")[0];
			$affelcted = DB::update("UPDATE pages SET content_en = '{$request->input('content_en')}' WHERE id = {$pageId}")[0];
			$affelcted = DB::update("UPDATE pages SET content_ru = '{$request->input('content_ru')}' WHERE id = {$pageId}")[0];
			$page = DB::select("SELECT name FROM pages WHERE id = {$pageId}")[0];
			$url = "/admin/{$page->name}";
			return redirect($url);
		}
		$url = "/admin/bach-flowers";
		return redirect($url);
	}
	
	public function contacts() {
		if (!Auth::check()) { return redirect('login'); }
		AdminController::langURL();
		AdminController::transl();
		// Active menu element
		$currentUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $currentUrl);
		if(isset($array[2])) $currentUrl = $array[2]; else $currentUrl = 'contacts';
		$transp = DB::select("SELECT * FROM transports ORDER BY type");
		
		return view('admin.contacts', [
			'menu' => $this->menu,
			'lvUrl' => $this->lvUrl,
			'enUrl' => $this->enUrl,
			'ruUrl' => $this->ruUrl,
			'lang' => $this->language,
			'footer' => $this->footerTransl,
			'transp' => $transp,
			'currentUrl' => $currentUrl,
		]);
	}
	public function contactsSave(Request $request) {
		if (!Auth::check()) { return redirect('login'); }
		$affected = DB::table("contacts")
			->where('id', '1')
			->update([
				'phone_number' => $request->input('Telefons'),
				'email' => $request->input('E-pasts'),
				'address' => $request->input('Adrese'),
			]);
		return redirect('/admin/contacts');
	}
	public function stopEdit(Request $request) {
		if (!Auth::check()) { return redirect('login'); }
		if (isset($_GET['id']) && ($transpId = intval($_GET['id'])) > 0) {
			$affected = DB::table("transports")
				->where('id', $transpId)
				->update([
					'type' => (int)$request->input('type'),
					'stop' => $request->input('stop'),
					'numbers' => $request->input('numbers'),
				]);
		} else if (isset($_GET['id']) && ($transpId = intval($_GET['id'])) == 0) {
			$type = (int)$request->input('type');
			$stop = $request->input('stop');
			$numbers = $request->input('numbers');
			$affected = DB::insert("INSERT INTO transports (type, stop, numbers) VALUES ({$type}, '{$stop}', '{$numbers}')");
		}
		return redirect('/admin/contacts');
	}
	public function stopDelete(Request $request) {
		if (!Auth::check()) { return redirect('login'); }
		if (isset($_GET['id']) && ($transpId = intval($_GET['id'])) > 0) {
			$affected = DB::delete("DELETE FROM transports WHERE id = {$transpId}");
		}
		return redirect('/admin/contacts');
	}
	
	public function users() {
		if (!Auth::check()) { return redirect('login'); }
		AdminController::langURL();
		AdminController::transl();
		// Active menu element
		$currentUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $currentUrl);
		if(isset($array[2])) $currentUrl = $array[2]; else $currentUrl = 'users';
		$users = DB::select("SELECT * FROM users ORDER BY name");
		
		return view('admin.users', [
			'menu' => $this->menu,
			'lvUrl' => $this->lvUrl,
			'enUrl' => $this->enUrl,
			'ruUrl' => $this->ruUrl,
			'lang' => $this->language,
			'footer' => $this->footerTransl,
			'users' => $users,
			'currentUrl' => $currentUrl,
		]);
	}
	public function userSave(Request $request) {
		if (!Auth::check()) { return redirect('login'); }
		if (isset($_GET['id']) && ($userId = intval($_GET['id'])) > 0) {
			$super_user = 0;
			if(isset($_POST['super_user'])) {
				$super_user = 1;
			}
			$affected = DB::table("users")
				->where('id', $userId)
				->update([
					'name' => $request->input('name'),
					'email' => $request->input('email'),
					'super_user' => $super_user,
				]);
		}
		return redirect('/admin/users');
	}
	public function userDelete(Request $request) {
		if (!Auth::check()) { return redirect('login'); }
		if (isset($_GET['id']) && ($userId = intval($_GET['id'])) > 0) {
			$user = DB::select("SELECT * FROM users WHERE id = {$userId}")[0];
			if ($user->super_user != 1) $affected = DB::delete("DELETE FROM users WHERE id = {$userId}");
		}
		return redirect('/admin/users');
	}
	
	
	
	public function langURL() {
		// LV url
		$this->lvUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $this->lvUrl);
		if($array[1] != 'lv') $array[1] = 'lv';
		$this->lvUrl = implode('/', $array);
		// EN url
		$this->enUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $this->enUrl);
		if($array[1] != 'en') $array[1] = 'en';
		$this->enUrl = implode('/', $array);
		// RU url
		$this->ruUrl = $_SERVER['REQUEST_URI'];
		$array = explode('/', $this->ruUrl);
		if($array[1] != 'ru') $array[1] = 'ru';
		$this->ruUrl = implode('/', $array);		
	}
	public function transl() {
		$this->footerTransl = DB::select("SELECT name as info, name_{$this->language} as infoKey FROM transl");
		foreach ($this->footerTransl as $elemKey => $elem) {
			if (Schema::hasColumn("contacts", "{$elem->info}")) {
				$elem->info = DB::select("SELECT {$elem->info} as infoVal FROM contacts");
			} else {
				unset($this->footerTransl[$elemKey]);
			}
		}
	}
}
